import type { LayoutLoad } from './$types'
import { defaultLanguage, supportedLanguages } from '$lib/common/domain/configurations'
import i18next from 'i18next'
import { translations as huTrans } from '$lib/common/translations/hu'
import { translations as roTrans } from '$lib/common/translations/ro'
import { translations as enTrans } from '$lib/common/translations/en'
import { currentLanguage, findLanguageByCode } from '$lib/common/domain/languages'
import { goto } from '$app/navigation'

export const ssr = false

export const load: LayoutLoad = async ({ params, url }) => {
  if (!params.lang) {
    return goto(`/${defaultLanguage.code}/lessons`)
  }

  if (!languageIsSupported(params.lang)) {
    return goto(`/${defaultLanguage.code}/lessons`)
  }

  const path = url.pathname.split('/')

  if (path.length === 2) {
    return goto(`/${params.lang}/lessons`)
  }

  if (!i18next.isInitialized) {
    const language = findLanguageByCode(params.lang)

    currentLanguage.set(language)

    await i18next.init({
      lng: params.lang,
      debug: false,
      resources: {
        hu: {
          translation: huTrans,
        },
        ro: {
          translation: roTrans,
        },
        en: {
          translation: enTrans,
        },
      },
    })
  }

  return {
    lang: params.lang,
    language: findLanguageByCode(params.lang),
  }
}

function languageIsSupported(langCode: string) {
  return supportedLanguages.reduce((carry, language) => carry || language.code === langCode, false)
}
