import type { LayoutLoad } from '../../../$types'
import { goto } from '$app/navigation'

export const ssr = false

export type TabLinks = {
  downloadLink: {
    selected: boolean
    href: string
  }
  uploadLink: {
    selected: boolean
    href: string
  }
}

export const load: LayoutLoad<TabLinks> = async ({ url }) => {
  if (url.pathname.split('/').pop() === 'backup') {
    await goto(`${url.pathname}/download${url.search}`)
  }

  const parentPath = url.pathname.split('/').slice(0, -1).join('/')

  const downloadHref = `${parentPath}/download${url.search}`
  let downloadIsSelected = true

  const uploadHref = `${parentPath}/upload${url.search}`
  let uploadIsSelected = false

  const currentPath = url.pathname.split('/').pop()

  if (currentPath === 'upload') {
    downloadIsSelected = false
    uploadIsSelected = true
  }

  return {
    downloadLink: {
      selected: downloadIsSelected,
      href: downloadHref,
    },
    uploadLink: {
      selected: uploadIsSelected,
      href: uploadHref,
    },
  }
}
