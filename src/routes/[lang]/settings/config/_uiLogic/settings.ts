import { getLessonConfigId } from '$lib/config/storage/configLocalStorage'
import { techniquesStorageId } from '$lib/lessons/domain/techniques'

const entriesToNotDelete = ['circuits', getLessonConfigId(), techniquesStorageId]

export function deleteAllLessonData() {
  Object.keys(localStorage).forEach((value) => {
    if (!entriesToNotDelete.includes(value)) {
      localStorage.removeItem(value)
    }
  })
}
