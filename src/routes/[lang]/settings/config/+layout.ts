import type { LayoutLoad } from '../../../$types'
import { goto } from '$app/navigation'

export const ssr = false

export type TabLinks = {
  lessonEdit: {
    selected: boolean
    href: string
  },
  lessonDelete: {
    selected: boolean
    href: string
  },
  lessonItemDuration: {
    selected: boolean
    href: string
  },
  lessonNavigation: {
    selected: boolean
    href: string
  }
}

type TabLinksKeys = keyof TabLinks

const paths: Record<TabLinksKeys, boolean> = {
  lessonEdit: false,
  lessonDelete: false,
  lessonItemDuration: false,
  lessonNavigation: false,
}

export const load: LayoutLoad<TabLinks> = async ({ url }) => {
  if (url.pathname.split('/').pop() === 'config') {
    await goto(`${url.pathname}/lessonEdit${url.search}`)
  }

  const parentPath = url.pathname.split('/').slice(0, -1).join('/')

  const currentPath = url.pathname.split('/').pop()

  return Object.entries(paths).reduce((previousValue, currentValue) => {
    const path = currentValue[0] as TabLinksKeys
    const isSelected = currentPath === path

    previousValue[path] = {
      selected: isSelected,
      href: `${parentPath}/${currentValue[0]}${url.search}`,
    }

    return previousValue
  }, {} as TabLinks)
}
