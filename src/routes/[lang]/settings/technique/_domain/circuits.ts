export type Duration = {
  minute: number
  second: number
}

export type CircuitItem = {
  id: string
  techniqueId: string
  duration: Duration
  isSoundEnabled: boolean
}

export type Circuit = {
  id: string
  name: string
  items: CircuitItem[][]
}

export function calculateDuration(circuit: Circuit): Duration {
  const duration = circuit.items.reduce((carry, circuitItems) => carry + calculateGroupDuration(circuitItems), 0)

  const second = duration % 60
  return {
    minute: (duration - second) / 60,
    second,
  }
}

function calculateGroupDuration(circuitItems: CircuitItem[]): number {
  return circuitItems.reduce(
    (carry, currentValue) => carry + currentValue.duration.minute * 60 + currentValue.duration.second,
    0
  )
}
