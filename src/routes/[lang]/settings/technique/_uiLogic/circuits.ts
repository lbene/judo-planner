import { get, writable } from 'svelte/store'
import type { Circuit, CircuitItem } from '../_domain/circuits'
import type { TemporaryLesson } from '$lib/lessons/components/lessonNavigation/presentation'
import {
  deleteById,
  loadAll,
  loadTempCircuit,
  resetTempCircuit,
  saveExistingCircuit,
  saveNewCircuit,
  saveTempCircuit,
} from '$lib/lessons/storage/circuitLocalStorage'

export const circuitStore = writable<Circuit[]>(loadAll().sort(compare))
export const temporaryCircuit = writable<Circuit | null>(loadTemporaryCircuit())

export function addCircuit(name: string, circuit: Circuit) {
  const newCircuit: Circuit = {
    ...circuit,
    name,
  }

  circuitStore.set(saveNewCircuit(newCircuit).sort(compare))

  resetTempCircuit()
  temporaryCircuit.set(null)
}

export function editCircuit(newName: string, circuit: Circuit, circuitId: string) {
  const updatedCircuit: Circuit = {
    ...circuit,
    id: circuitId,
    name: newName,
  }

  circuitStore.set(saveExistingCircuit(updatedCircuit).sort(compare))

  resetTempCircuit()
  temporaryCircuit.set(null)
}

export function loadTemporaryCircuit(): Circuit | null {
  return loadTempCircuit()
}

export function saveTemporaryCircuit(name: string, lessonItems: TemporaryLesson[], groupNumber: number) {
  const currentTemp = get(temporaryCircuit)

  let updatedItems = []

  if (!currentTemp || currentTemp.items.length === 0) {
    const newValue = lessonItems.map<CircuitItem>((item) => ({
      id: crypto.randomUUID(),
      techniqueId: item.id,
      duration: {
        minute: item.minute,
        second: item.second,
      },
      isSoundEnabled: false,
    }))

    updatedItems.push(newValue)
  } else {
    updatedItems = [...currentTemp.items]

    updatedItems[groupNumber] = [
      ...currentTemp.items[groupNumber],
      ...lessonItems.map<CircuitItem>((item) => ({
        id: crypto.randomUUID(),
        techniqueId: item.id,
        duration: {
          minute: item.minute,
          second: item.second,
        },
        isSoundEnabled: false,
      })),
    ]
  }

  const circuit: Circuit = {
    ...currentTemp,
    id: crypto.randomUUID(),
    name,
    items: updatedItems,
  }

  temporaryCircuit.set(saveTempCircuit(circuit))
}

export function removeGeneratedSeries() {
  const currentTemp = get(temporaryCircuit)

  if (currentTemp) {
    currentTemp.items = [currentTemp.items[0]]

    temporaryCircuit.set(saveTempCircuit(currentTemp))
  }
}

export function revertToOriginal(id: string) {
  resetTempCircuit()

  temporaryCircuit.set(loadCircuitById(id) ?? null)
}

export function updateTemporaryCircuit(name: string, lessonItems: CircuitItem[][]) {
  const currentTemp = get(temporaryCircuit)

  if (!currentTemp) {
    throw new Error('Could not update temporary circuit, because it does not exist')
  }

  const circuit: Circuit = {
    ...currentTemp,
    name,
    items: lessonItems,
  }

  temporaryCircuit.set(saveTempCircuit(circuit))
}

export function updateDuration(groupNumber: number, item: CircuitItem, minute: number, second: number) {
  const currentTemp = get(temporaryCircuit)

  if (currentTemp) {
    const updatedItem: CircuitItem = {
      ...item,
      duration: {
        minute,
        second,
      },
    }

    currentTemp.items[groupNumber] = currentTemp.items[groupNumber].map((value) => {
      if (value.id === item.id) {
        return updatedItem
      }
      return value
    })

    temporaryCircuit.set(saveTempCircuit(currentTemp))
  }
}

export function updateOrder(groupNumber: number, itemIds: Record<string, number>) {
  const currentTemp = get(temporaryCircuit)

  if (currentTemp) {
    currentTemp.items[groupNumber].sort((a, b) => itemIds[a.id] - itemIds[b.id])
    temporaryCircuit.set(saveTempCircuit(currentTemp))
  }
}

export function deleteExercise(groupNumber: number, itemId: string) {
  const currentTemp = get(temporaryCircuit)

  if (currentTemp) {
    currentTemp.items[groupNumber] = currentTemp.items[groupNumber].filter((value) => value.id !== itemId)

    temporaryCircuit.set(saveTempCircuit(currentTemp))
  }
}

export function cloneExercise(groupNumber: number, item: CircuitItem) {
  const currentTemp = get(temporaryCircuit)

  if (currentTemp) {
    currentTemp.items[groupNumber].push({
      ...item,
      id: crypto.randomUUID(),
    })

    temporaryCircuit.set(saveTempCircuit(currentTemp))
  }
}
export function toggleSoundEffect(groupNumber: number, item: CircuitItem, soundEffect: boolean) {
  const currentTemp = get(temporaryCircuit)

  if (currentTemp) {
    const items = currentTemp.items.map((value) => {
      return value.map((localItem) => {
        if (localItem.techniqueId === item.techniqueId) {
          return {
            ...localItem,
            isSoundEnabled: soundEffect,
          }
        }

        return localItem
      })
    })

    temporaryCircuit.set(
      saveTempCircuit({
        ...currentTemp,
        items,
      })
    )
  }
}

export function deleteCircuit(id: string) {
  circuitStore.set(deleteById(id))
}

export function loadCircuitById(id: string): Circuit | undefined {
  const circuit = loadAll()
    .sort(compare)
    .find((value) => value.id === id)

  if (circuit) {
    return { ...circuit }
  }
}

function compare(a: Circuit, b: Circuit) {
  return a.name < b.name ? -1 : a.name > b.name ? 1 : 0
}
