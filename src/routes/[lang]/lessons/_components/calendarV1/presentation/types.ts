import type { Option } from '$lib/common/types/types'

export type CalendarFilterEvent = {
  update: FilterDetails
}

type FilterTypes = 'year' | 'month' | 'week'

export type FilterDetails = {
  name: FilterTypes
  value: string
}

export type CalendarFilterType = {
  defaultValue: Option
  options: Option[]
}

export type CalendarFiltersProps = {
  [key in FilterTypes]: CalendarFilterType
}
