import { derived } from 'svelte/store'
import { currentLesson } from '$lib/lessons/uiLogic/lessonStore'
import { type Optional, optionalOf } from '$lib/common/domain/optional'
import { loadHistory } from '$lib/lessons/storage/historyLocalStorage'
import type { Lesson, LessonItem } from '$lib/lessons/domain/lessons'
import type { LessonHistory } from '$lib/lessons/domain/player'
import { StatusValues } from '$lib/lessons/domain/player'
import type { Circuit } from '../../settings/technique/_domain/circuits'
import { getLessonBreakdown, type LessonBreakdown } from '$lib/lessons/domain/lesson'

export const lessonHistory = derived(currentLesson, (lesson) => {
  return optionalOf(lesson).flatMap((lesson) => loadHistory(lesson.id))
})

export function getHistoryIndicator(
  lessonItem: LessonItem,
  history: Optional<LessonHistory>,
  circuit: Optional<Circuit>
): StatusValues {
  return circuit
    .map((circuit) => getCircuitStatus(circuit, history, lessonItem))
    .orElse(getLessonItemStatus(lessonItem, history))
    .getOrElse(StatusValues.NOT_STARTED)
}

export function getHistoryIndicatorForCircuitItems(
  circuit: Circuit,
  history: Optional<LessonHistory>,
  lessonItemId: string
) {
  const circuitItemIds = getCircuitItemIds(circuit)

  return circuitItemIds.reduce<Record<string, StatusValues>>((statuses, id) => {
    const statusId = `${lessonItemId}+${id}`
    const status = history
      .flatMap<StatusValues>((history) => optionalOf(history.statuses[statusId]))
      .getOrElse(StatusValues.NOT_STARTED)

    return {
      ...statuses,
      [id]: status,
    }
  }, {})
}

export function getBreakdown(lesson: Optional<Lesson>): LessonBreakdown {
  return lesson
    .map((value) => getLessonBreakdown(value))
    .getOrElse({
      totalActiveTime: 0,
      totalPauseTime: 0,
      totalTime: 0,
    })
}

function getLessonItemStatus(lessonItem: LessonItem, history: Optional<LessonHistory>) {
  return history.flatMap<StatusValues>((history) => {
    return optionalOf(history.statuses[lessonItem.id])
  })
}

function getCircuitStatus(circuit: Circuit, history: Optional<LessonHistory>, lessonItem: LessonItem): StatusValues {
  const circuitItemIds = getCircuitItemIds(circuit)

  const circuitStatus = circuitItemIds.reduce<StatusValues>((previousValue, id) => {
    const statusId = `${lessonItem.id}+${id}`
    return (
      previousValue +
      history
        .flatMap<StatusValues>((history) => optionalOf(history.statuses[statusId]))
        .getOrElse(StatusValues.NOT_STARTED)
    )
  }, 0)

  const notStartedAtAll = -circuitItemIds.length
  const completelyFinished = circuitItemIds.length

  if (circuitStatus === notStartedAtAll) {
    return StatusValues.NOT_STARTED
  }

  if (circuitStatus === completelyFinished) {
    return StatusValues.FINISHED
  }

  return StatusValues.STARTED
}

function getCircuitItemIds(circuit: Circuit) {
  return circuit.items.flatMap((circuitItemsPerGroup) => circuitItemsPerGroup.map((circuitItem) => circuitItem.id))
}
