import type { Option } from '$lib/common/types/types'
import type { Language } from '$lib/common/domain/configurations'
import { years } from '$lib/common/domain/configurations'
import type { Page } from '@sveltejs/kit'
import { getDaysByWeek, getMonths } from '$lib/common/domain/common'
import { t } from 'i18next'
import type { CalendarFiltersProps } from '../../_components/calendarV1/presentation/types'

type Day = { date: number; name: string }
export type DaysByWeek = Record<number, Day[]>

export function getDefaultYearOption(): Option {
  const today = new Date()

  const year = today.getFullYear().toString()

  const option = years.find((value) => value.value === year)

  if (!option) {
    throw new Error(`Year option ${year} does not exist on default, please update year list`)
  }

  return option
}

export function updateYearOption(page: Page): Option {
  const year = page.url.searchParams.get('year')

  if (!year) {
    return getDefaultYearOption()
  }

  const option = years.find((value) => value.value === year)

  if (!option) {
    throw new Error(`Year option ${year} does not exist on update, please update year list`)
  }

  return option
}

export function getDefaultMonthOption(language: Language): Option {
  const months = getMonths(language.isoCode).map((value, i) => ({
    value: i + '',
    label: value,
  }))

  const today = new Date()

  const month = today.getMonth()

  return {
    value: months[month].value,
    label: months[month].label,
  }
}

export function updateMonthOption(language: Language, page: Page): Option {
  const monthValueFromParam = page.url.searchParams.get('month')

  if (!monthValueFromParam) {
    return getDefaultMonthOption(language)
  }

  const months = getMonths(language.isoCode).map((value, i) => ({
    value: i + '',
    label: value,
  }))

  const month = Number(monthValueFromParam) - 1

  return {
    value: months[month].value,
    label: months[month].label,
  }
}

export function getDefaultWeekOption(): Option {
  const today = new Date()

  const week = Object.entries(getDaysByWeek(today.getFullYear().toString(), today.getMonth().toString())).find(
    (value) => {
      const day = value[1].find((value) => value.date === today.getDate())

      return !!day
    }
  )

  if (!week) {
    return {
      value: '1',
      label: t('weekNumber', { number: '1' }),
    }
  }

  return {
    value: week[0],
    label: t('weekNumber', { number: week[0] }),
  }
}

export function updateWeekOption(page: Page, numberOfWeeks: number): Option {
  let week = page.url.searchParams.get('week')

  if (!week) {
    week = getDefaultWeekOption().value
  }

  const clampedWeek = Math.min(Number(week), numberOfWeeks)

  return {
    value: clampedWeek.toString(),
    label: t('weekNumber', { number: clampedWeek }),
  }
}

export function getMonthOptions(language: Language) {
  return getMonths(language.isoCode).map((value, i) => ({
    value: i + '',
    label: value,
  }))
}

export function getWeekOptions() {
  const today = new Date()

  const daysByWeek = getDaysByWeek(today.getFullYear().toString(), today.getMonth().toString())

  return Object.keys(daysByWeek).map<Option>((value) => ({
    value,
    label: t('weekNumber', { number: value }),
  }))
}

function updateWeekOptions(year: string, month: string) {
  const daysByWeek = getDaysByWeek(year, month)

  return Object.keys(daysByWeek).map<Option>((value) => ({
    value,
    label: t('weekNumber', { number: value }),
  }))
}

export function getFilterProps(language: Language): CalendarFiltersProps {
  return {
    year: {
      defaultValue: getDefaultYearOption(),
      options: years,
    },
    month: {
      defaultValue: getDefaultMonthOption(language),
      options: getMonthOptions(language),
    },
    week: {
      defaultValue: getDefaultWeekOption(),
      options: getWeekOptions(),
    },
  }
}

export function updateFilterProps(page: Page, language: Language): CalendarFiltersProps {
  const year = updateYearOption(page)
  const month = updateMonthOption(language, page)
  const weekOptions = updateWeekOptions(year.value, month.value)

  return {
    year: {
      defaultValue: year,
      options: years,
    },
    month: {
      defaultValue: month,
      options: getMonthOptions(language),
    },
    week: {
      defaultValue: updateWeekOption(page, weekOptions.length),
      options: weekOptions,
    },
  }
}

export function getDays(): Day[] {
  const today = new Date()

  const daysByWeek = getDaysByWeek(today.getFullYear().toString(), today.getMonth().toString())

  const week = Object.entries(daysByWeek).find((value) => {
    const day = value[1].find((value) => value.date === today.getDate())

    return !!day
  })

  const currentWeek: number = !week ? 1 : Number(week[0])

  return daysByWeek[currentWeek]
}

export function updateDays(page: Page, language: Language): Day[] {
  const year = updateYearOption(page)
  const month = updateMonthOption(language, page)
  const daysByWeek = getDaysByWeek(year.value, month.value)

  const weekOptions = updateWeekOptions(year.value, month.value)
  const week = updateWeekOption(page, weekOptions.length).value

  return daysByWeek[Number(week)]
}
