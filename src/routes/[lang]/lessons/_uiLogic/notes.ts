import type { Note, NoteId } from '$lib/lessons/domain/history'
import type { Optional } from '$lib/common/domain/optional'
import type { LessonHistory } from '$lib/lessons/domain/player'
import { saveHistory } from '$lib/lessons/storage/historyLocalStorage'

export function addNoteToHistory(history: Optional<LessonHistory>, noteContent: string): Note[] {
  const result = history
    .map((value) => {
      const note: Note = {
        id: crypto.randomUUID(),
        content: noteContent,
        createdAt: new Date(),
        updatedAt: new Date(),
      }

      value.notes = [...value.notes, ...[note]]

      return value
    })
    .getOrThrow(`Could not find history when adding note`)

  saveHistory(result)

  return result.notes
}

export function updateNote(history: Optional<LessonHistory>, noteId: NoteId, noteContent: string): Note[] {
  const result = history
    .map((value) => updateLessonWithNote(value, noteId, noteContent))
    .getOrThrow(`Could not find history when updating note`)

  saveHistory(result)

  return result.notes
}

export function deleteNote(history: Optional<LessonHistory>, noteId: NoteId) {
  const result = history
    .map((value) => deleteNoteFromHistory(value, noteId))
    .getOrThrow(`Could not find history when updating note`)

  saveHistory(result)

  return result.notes
}

function updateLessonWithNote(history: LessonHistory, noteId: NoteId, noteContent: string): LessonHistory {
  const notes = history.notes.map((note) => {
    if (note.id === noteId) {
      return {
        ...note,
        updatedAt: new Date(),
        content: noteContent,
      }
    }

    return note
  })

  history.notes = [...notes]

  return history
}

function deleteNoteFromHistory(history: LessonHistory, noteId: NoteId): LessonHistory {
  history.notes = history.notes.filter((note) => note.id !== noteId)

  return history
}
