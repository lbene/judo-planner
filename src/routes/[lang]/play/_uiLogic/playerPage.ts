import type { LessonItem } from '$lib/lessons/domain/lessons'
import { type Optional, optionalOf } from '$lib/common/domain/optional'
import type { Circuit } from '../../settings/technique/_domain/circuits'
import type { PlayableLessonItems } from './playableLessonItems'

export function lessonItemIsNotCircuit(lessonItems: PlayableLessonItems, lessonItem: LessonItem): boolean {
  const circuit: Optional<Circuit> = Object.values(lessonItems).reduce((previousValue, currentValue) => {
    if (currentValue.lessonItem.id === lessonItem.id && currentValue.circuit) {
      return currentValue.circuit
    }

    return previousValue
  }, optionalOf<Circuit>(null))

  return circuit.isNone()
}

export function getCircuit(lessonItems: PlayableLessonItems, lessonItem: LessonItem): Circuit {
  const circuit: Optional<Circuit> = Object.values(lessonItems).reduce((previousValue, currentValue) => {
    if (currentValue.lessonItem.id === lessonItem.id && currentValue.circuit) {
      return currentValue.circuit
    }

    return previousValue
  }, optionalOf<Circuit>(null))

  return circuit.getOrThrow('Should not be called, because of the guard in the template')
}
