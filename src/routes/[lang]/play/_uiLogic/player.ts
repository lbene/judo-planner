import type { Lesson, LessonItem } from '$lib/lessons/domain/lessons'
import type { Circuit } from '../../settings/technique/_domain/circuits'
import type { Readable, Writable } from 'svelte/store'
import { get, writable } from 'svelte/store'
import { getLessonItemDuration, isSoundEnabled } from '$lib/lessons/domain/lesson'
import type { PlayableLessonItemId } from '$lib/lessons/domain/player'
import { StatusValues } from '$lib/lessons/domain/player'
import type { Optional } from '$lib/common/domain/optional'
import { optionalOf } from '$lib/common/domain/optional'
import type { PlayableLessonItems, PlayableLessonItem } from './playableLessonItems'
import { createHistory, updateHistory } from './history'
import sound from '$lib/assets/sound.mp3'
import lessonEnd from '$lib/assets/lessonEnd.mp3'

type LessonCountdown = {
  chapterTitle: string
  lessonItemTitle: string
  circuit: Optional<Circuit>
  remainingTime: number
  groupNumber: Optional<number>
  nextLessonItemTitle: Optional<string>
}

type GlobalCountdown = {
  elapsed: number
  remaining: number
}

export interface WithMainCountdownStore {
  getMainCountdownStore: () => Readable<LessonCountdown>
}

export interface WithStatusStore {
  getStatusStore: () => Readable<CurrentStatus>
}

export interface WithGlobalTimerStore {
  getGlobalTimerStore: () => Readable<GlobalCountdown>
}

export interface WithPlayerOperations {
  continue: () => void
  restart: () => void
  pause: () => void
  stop: () => void
}

export interface WithJumpOperation {
  jumpTo: (lessonItemId: string) => void
}

type CurrentStatus = {
  status: 'progress' | 'finished'
  currentStatus: StatusValues
  current: PlayableLessonItem
  currentIndex: number
  currentId: PlayableLessonItemId
  next: Optional<PlayableLessonItem>
}

const countdown = new Audio(sound)
const finalCountdown = new Audio(lessonEnd)

export function startPlayer(lessonItems: PlayableLessonItems, lesson: Lesson): Player {
  createHistory(lesson, lessonItems)

  const lessonItemsList = Object.values(lessonItems)
  const firstLessonItem = lessonItemsList[0]

  const lessonDuration = lesson.chapters.reduce((carry, chapter) => {
    return (
      carry + chapter.lessonItems.reduce((carry, lessonItem) => carry + getLessonItemDuration(lessonItem.duration), 0)
    )
  }, 0)

  const countdown: LessonCountdown = {
    chapterTitle: firstLessonItem.chapter.title,
    lessonItemTitle: firstLessonItem.name,
    remainingTime: firstLessonItem.duration,
    circuit: firstLessonItem.circuit,
    groupNumber: firstLessonItem.groupNumber,
    nextLessonItemTitle: optionalOf(lessonItemsList[1]?.name),
  }

  const initialStatus: CurrentStatus = {
    status: 'progress',
    currentStatus: StatusValues.STARTED,
    current: firstLessonItem,
    currentIndex: 0,
    currentId: firstLessonItem.id,
    next: optionalOf(lessonItemsList[1]),
  }

  const globalCountdown: GlobalCountdown = {
    elapsed: 0,
    remaining: lessonDuration,
  }

  return new Player(countdown, initialStatus, globalCountdown, lesson, lessonItemsList, firstLessonItem.id, lessonItems)
}

class Player
  implements WithPlayerOperations, WithJumpOperation, WithMainCountdownStore, WithStatusStore, WithGlobalTimerStore
{
  lessonCountdown: Writable<LessonCountdown>
  currentStatus: Writable<CurrentStatus>
  globalCountdown: Writable<GlobalCountdown>

  lessonTimer = 0
  globalTimer = 0

  constructor(
    lessonCountdown: LessonCountdown,
    initialStatus: CurrentStatus,
    globalCountdown: GlobalCountdown,
    private lesson: Lesson,
    private lessonItemsList: PlayableLessonItem[],
    private firstLessonItemId: string,
    private lessonItems: PlayableLessonItems
  ) {
    this.lessonCountdown = writable(lessonCountdown)
    this.currentStatus = writable(initialStatus)
    this.globalCountdown = writable(globalCountdown)

    // The lesson item specific timer needs to be separate from the global one, because if we hit stop
    // and then retry, there's no guarantee that we will have a full second after the timer is reset on retry.
    this.startGlobalTimer()
    this.startLessonTimer()
    updateHistory(this.lesson.id, this.firstLessonItemId, StatusValues.STARTED)
  }

  startGlobalTimer() {
    this.globalTimer = setInterval(() => {
      this.globalCountdown.update((value) => {
        return {
          elapsed: value.elapsed + 1,
          remaining: value.remaining - 1,
        }
      })
    }, 1000) as unknown as number
  }

  startLessonTimer(): number {
    this.lessonTimer = setInterval(() => {
      this.lessonCountdown.update((lessonCountdown) => {
        // We skip second 0, because that adds a second to the global count

        const currentLessonItem = this.getCurrentStatus().current.lessonItem

        if (isSoundEnabled(currentLessonItem) && lessonCountdown.remainingTime === 7) {
          countdown.play()
        }

        if (isSoundEnabled(currentLessonItem) && lessonCountdown.remainingTime === 2) {
          finalCountdown.play()
        }

        if (lessonCountdown.remainingTime - 1 === 0) {
          this.currentStatus.update((status) => {
            return this.updateStatus({ ...status })
          })

          const status = this.getCurrentStatus()

          if (status.status === 'finished') {
            lessonCountdown.remainingTime = 0
            return lessonCountdown
          }

          return {
            chapterTitle: status.current.chapter.title,
            lessonItemTitle: status.current.name,
            remainingTime: status.current.duration,
            circuit: status.current.circuit,
            groupNumber: status.current.groupNumber,
            nextLessonItemTitle: status.next.map((value) => value.name),
          }
        }

        // This does not need to check if it's paused anymore, because pause and stop operations
        // will stop the timer with clearInterval
        lessonCountdown.remainingTime = lessonCountdown.remainingTime - 1

        return lessonCountdown
      })
    }, 1000) as unknown as number

    return this.lessonTimer
  }

  private getCurrentStatus(): CurrentStatus {
    return get(this.currentStatus)
  }

  restart() {
    const status = this.getCurrentStatus()

    clearInterval(this.lessonTimer)

    this.lessonCountdown.update((value) => {
      value.remainingTime = status.current.duration

      return value
    })

    // We start lesson timer again, which resets the history, so that we can get accurate status
    this.startLessonTimer()
  }

  continue() {
    // We start lesson timer again, which resets the history, so that we can get accurate status
    this.startLessonTimer()
  }

  jumpTo(lessonItemId: string) {
    const lessonItems = Object.entries(this.lessonItems)

    const { currentIndex, lessonItem, next } = this.getLessonItemsForStatus(lessonItems, lessonItemId)

    this.currentStatus.set({
      current: lessonItem,
      currentIndex,
      currentId: lessonItemId,
      next,
      status: 'progress',
      currentStatus: StatusValues.STARTED,
    })

    this.lessonCountdown.update(() => {
      return {
        chapterTitle: lessonItem.chapter.title,
        lessonItemTitle: lessonItem.name,
        remainingTime: lessonItem.duration,
        circuit: lessonItem.circuit,
        isNotPaused: true,
        groupNumber: lessonItem.groupNumber,
        nextLessonItemTitle: next.map((value) => value.name),
      }
    })

    clearInterval(this.lessonTimer)

    // We start lesson timer again, which resets the history, so that we can get accurate status
    this.startLessonTimer()
  }

  private getLessonItemsForStatus(
    lessonItems: [PlayableLessonItemId, PlayableLessonItem][],
    lessonItemId: string
  ): { currentIndex: number; lessonItem: PlayableLessonItem; next: Optional<PlayableLessonItem> } {
    let next = optionalOf<PlayableLessonItem>(null)
    let currentIndex = 0
    let id = lessonItems[currentIndex][0]
    let lessonItem = lessonItems[currentIndex][1]

    for (currentIndex = 0; currentIndex < lessonItems.length; currentIndex++) {
      id = lessonItems[currentIndex][0]
      lessonItem = lessonItems[currentIndex][1]

      // The first condition is used to check for clicks that happen on an individual circuit item.
      // The second case is used to check for clicks that happen on lesson elements including circuits
      if (lessonItemId === id || lessonItemId === lessonItem.id) {
        next = optionalOf(lessonItems[currentIndex + 1]).map((tuple) => tuple[1])

        break
      }
    }

    return { currentIndex, lessonItem, next }
  }

  pause() {
    clearInterval(this.lessonTimer)
  }

  stop() {
    clearInterval(this.lessonTimer)
    clearInterval(this.globalTimer)
  }

  getMainCountdownStore() {
    return this.lessonCountdown
  }

  getStatusStore(): Readable<CurrentStatus> {
    return this.currentStatus
  }

  getGlobalTimerStore(): Readable<GlobalCountdown> {
    return this.globalCountdown
  }

  private updateStatus(status: CurrentStatus) {
    const nextIndex = status.currentIndex + 1

    updateHistory(this.lesson.id, status.current.id, StatusValues.FINISHED)

    // TODO implement with map
    if (status.next.isNone()) {
      status.status = 'finished'

      return status
    }

    if (this.lessonItemsList[nextIndex] && status.next.isSome()) {
      status.current = status.next.get()
      status.currentIndex = nextIndex
      status.currentId = status.current.id
      status.next = optionalOf(this.lessonItemsList[nextIndex + 1])
      status.currentStatus = StatusValues.STARTED

      updateHistory(this.lesson.id, status.currentId, StatusValues.STARTED)
    }

    return status
  }
}
