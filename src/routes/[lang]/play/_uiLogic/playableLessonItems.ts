import type { Lesson } from '$lib/lessons/domain/lessons'
import type { Circuit } from '../../settings/technique/_domain/circuits'
import type { Technique } from '$lib/lessons/domain/techniques'
import type { Chapter, LessonItem } from '$lib/lessons/domain/lessons'
import { type Optional, optionalOf } from '$lib/common/domain/optional'
import { type PlayableLessonItemId, StatusValues } from '$lib/lessons/domain/player'
import { getLessonItemDuration } from '$lib/lessons/domain/lesson'
import type { CircuitItem } from '../../settings/technique/_domain/circuits'
import { getTechniqueNames } from '$lib/lessons/domain/techniques'

type PlayableLessonItemStatus = {
  value: StatusValues
}

export type PlayableLessonItem = {
  id: PlayableLessonItemId
  chapter: Chapter
  lessonItem: LessonItem
  circuitItem: Optional<CircuitItem>
  circuit: Optional<Circuit>
  status: PlayableLessonItemStatus
  duration: number
  isSoundEnabled: boolean
  name: string
  groupNumber: Optional<number>
}

export type PlayableLessonItems = Record<PlayableLessonItemId, PlayableLessonItem>

/**
 * Creates a list of lesson items specific to the player, so that it can be processed for history, view
 * lesson items in the tab and operations in the player like, jump, pause, etc.
 *
 * It basically, flattens normal lesson items and circuits into a flat structure and adds status on it
 * and other metadata for simple player implementation.
 */
export function createPlayableLessonItems(
  lesson: Lesson,
  circuits: Circuit[],
  techniques: Technique[]
): PlayableLessonItems {
  const techniquesNames = getTechniqueNames(techniques)

  const items: PlayableLessonItems = {}

  for (const chapter of lesson.chapters) {
    for (const lessonItem of chapter.lessonItems) {
      const circuit = optionalOf(circuits.find((value) => value.id === lessonItem.techniqueId))

      const initialStatus: PlayableLessonItemStatus = {
        value: StatusValues.NOT_STARTED,
      }

      const lessonItems = circuit
        .map((value) => createPlayableLessonItemsFromCircuit(chapter, value, initialStatus, techniquesNames, lessonItem))
        .getOrElse([
          {
            id: lessonItem.id,
            chapter,
            lessonItem,
            circuit,
            status: initialStatus,
            duration: getLessonItemDuration(lessonItem.duration),
            isSoundEnabled: lessonItem.soundEffect ?? false,
            name: lessonItem.title,
            circuitItem: optionalOf<CircuitItem>(undefined),
            groupNumber: optionalOf<number>(undefined),
          },
        ])

      for (const item of lessonItems) {
        items[item.id] = item
      }
    }
  }

  return items
}

function createPlayableLessonItemsFromCircuit(
  chapter: Chapter,
  circuit: Circuit,
  initialStatus: PlayableLessonItemStatus,
  techniquesNames: Record<string, string>,
  lessonItem: LessonItem
) {
  const circuitItems: PlayableLessonItem[] = []

  for (let groupNumber = 0; groupNumber < circuit.items.length; groupNumber++) {
    for (const item of circuit.items[groupNumber]) {
      circuitItems.push({
        id: `${lessonItem.id}+${item.id}`,
        chapter,
        circuitItem: optionalOf(item),
        circuit: optionalOf(circuit),
        status: initialStatus,
        duration: getLessonItemDuration(item.duration),
        isSoundEnabled: item.isSoundEnabled,
        lessonItem,
        name: techniquesNames[item.techniqueId] ?? '',
        groupNumber: optionalOf(groupNumber),
      })
    }
  }

  return circuitItems
}
