import type { PlayableLessonItems } from './playableLessonItems'
import type { LessonHistory, PlayableLessonItemId, StatusValues } from '$lib/lessons/domain/player'
import { loadHistory, saveHistory } from '$lib/lessons/storage/historyLocalStorage'
import type { Lesson } from '$lib/lessons/domain/lessons'

export function createHistory(lesson: Lesson, lessonItems: PlayableLessonItems) {
  const statuses: Record<PlayableLessonItemId, StatusValues> = {}

  for (const id in lessonItems) {
    statuses[id] = lessonItems[id].status.value
  }

  const history: LessonHistory = {
    lessonId: lesson.id,
    startedAt: new Date(),
    statuses,
    notes: [],
  }

  saveHistory(history)
}

export function updateHistory(lessonId: string, id: PlayableLessonItemId, status: StatusValues) {
  const history = loadHistory(lessonId).getOrThrow(`History for ${lessonId} does not exist on update`)

  history.statuses[id] = status

  saveHistory(history)
}
