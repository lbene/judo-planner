import type { PageData, PageLoad } from './$types'
import { loadLesson } from '$lib/lessons/storage/lessonLocalStorage'
import type { Lesson } from '$lib/lessons/domain/lessons'
import { loadAllCircuits } from '$lib/lessons/storage/circuitLocalStorage'
import { loadTechniques } from '$lib/lessons/storage/techniquesLocalStorage'
import { type PlayableLessonItems, createPlayableLessonItems } from '../_uiLogic/playableLessonItems'

export const load: PageLoad<Data> = ({ params, url }) => {
  return Promise.all([loadLesson(params.lessonId), loadAllCircuits(), loadTechniques()]).then(
    ([lesson, circuits, techniques]) => {
      return {
        lesson,
        lessonItems: createPlayableLessonItems(lesson, circuits, techniques),
      }
    }
  )
}

// TODO update the Svelte plugin, this might not be needed anymore
type Data = {
  lessonItems: PlayableLessonItems
  lesson: Lesson
}

export type PlayerPageData = PageData & Data
