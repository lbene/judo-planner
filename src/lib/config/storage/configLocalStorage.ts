export interface LessonConfig {
  editPastLessons: boolean
  maxLessonItemDuration: number
  lessonNavigationVersion: LessonNavigationVersionValues
  getNavigationVersionValues(): LessonNavigationVersionValues[]
}

type LessonNavigationVersionValues = 'v1' | 'v2'

export function saveLessonConfig(lessonConfig: LessonConfig) {
  localStorage.setItem(getLessonConfigId(), JSON.stringify(lessonConfig))
}

export function loadLessonConfig(): LessonConfig {
  const rawConfig = localStorage.getItem(getLessonConfigId())

  if (!rawConfig) {
    return new LocalStorageConfig('{}')
  }

  return new LocalStorageConfig(rawConfig)
}

export function getLessonConfigId(): string {
  return 'lessonConfig'
}

class LocalStorageConfig implements LessonConfig {
  editPastLessons: boolean
  maxLessonItemDuration: number
  lessonNavigationVersion: LessonNavigationVersionValues

  constructor(rawConfig: string) {
    const object = JSON.parse(rawConfig) as Partial<LessonConfig>

    this.editPastLessons = object.editPastLessons ?? false
    this.maxLessonItemDuration = object.maxLessonItemDuration ?? 60
    this.lessonNavigationVersion = object.lessonNavigationVersion ?? 'v1'
  }

  getNavigationVersionValues(): LessonNavigationVersionValues[] {
    return [
      'v1',
      'v2'
    ]
  }
}
