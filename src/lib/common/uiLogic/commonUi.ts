export function enableGlobalScroll() {
  document.body.classList.remove('open-modal')
}

export function disableGlobalScroll() {
  document.body.classList.add('open-modal')
}

export function clickOutside(node: any) {
  const handleClick = (event: MouseEvent) => {
    if (node && !node.contains(event.target) && !event.defaultPrevented) {
      node.dispatchEvent(new CustomEvent('click_outside', node))
    }
  }

  document.addEventListener('click', handleClick, true)

  return {
    destroy() {
      document.removeEventListener('click', handleClick, true)
    },
  }
}

export function stopAudio(audio: HTMLAudioElement) {
  audio.pause()

  audio.currentTime = 0
}
