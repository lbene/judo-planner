export const translations = {
  weekNumber: 'Week {{number}}.',
  editModalTitle: 'Edit title',
  editDateModalTitle: 'Edit date',
  cloneLesson: 'Clone lesson',
  editModalSubmitLabel: 'Edit date',
  save: 'Save',
  dateIsTaken: 'This date is taken already. Please choose another.',
  dateIsInThePast: 'This date is in the past. Please choose another.',
  addChapterLabel: 'Add new chapter',
  addChapterModalSubmitLabel: 'Add',
  delete: 'Delete',
  deleteWarning: 'Are you sure you want to delete?',
  confirmDelete: 'Yes',
  cancelDelete: 'No',
  saveLessonItem: 'Save Lesson Item',
  saveCircuitItem: 'Save Circuit Item',
  lessons: 'Lessons',
  addTech: 'Add Waza',
  addCircuit: 'Add Circuit',
  techniques: 'Techniques',
  circuits: 'Circuits',
  name: 'Name',
  videoUrl: 'Media URL',
  wazaCatalog: 'Waza Catalog',
  editLessonDurationModalTitle: 'Edit duration',
  time: 'Time',
  cancel: 'Cancel',
  Szünet: 'Break',
  completed: 'Completed',
  notCompleted: 'Not completed',
  skipped: 'Skipped',
  report: 'Report',
  plan: 'Plan',
  totalActiveTime: 'Total active time',
  totalPauseTime: 'Total pause time',
  totalTime: 'Total time',
  remainingTime: 'Remaining time',
  elapsedTime: 'Elapsed time',
  setup: 'Setup',
  preparation: 'Preparation',
  fundamentals: 'Fundamentals',
  end: 'End',
  refreshWarning: 'Your history for this lesson will be deleted. Do you want to proceed?',
  download: 'Download',
  lessonDelete: 'Lesson Delete',
  lessonNavigation: 'Lesson Navigation',
  lessonEditing: 'Lesson Editing',
  lessonItemDuration: 'Lesson Item Duration',
  maxLessonItemDuration: 'Lesson Item Max Duration',
  editPastLessons: 'Edit past lessons',
  lessonAddDisabled: 'Adding lessons in the past is not possible',
  upload: 'Upload',
  backup: 'Backup',
  config: 'App Config',
  uploadWarning: 'Uploading a new file will cause all your previous data to be deleted',
  hasVideo: 'View video',
  hasImage: 'View image',
  clone: 'Clone',
  chapterDuration: 'Chapter duration',
  notes: 'Notes',
  newNote: 'New note',
  disableSoundEffect: 'Mute',
  enableSoundEffect: 'Unmute',
  removeSeries: 'Remove series',
  cancelAll: 'Cancel all',
  cancelSeriesGeneration: 'Cancel series',
  circuitName: 'Circuit name',
  numberOfSeries: 'Number of series',
  generate: 'Generate',
  groupDuration: 'Duration of series',
  endOfLesson: 'End of lesson',
  chapter: 'Chapter',
  circuit: 'Circuit',
  exercise: 'Exercise',
  next: 'Next',
  seriesNumberLabel: 'Series No',
  seriesNumberValue: '{{value}} / {{total}}',
}
