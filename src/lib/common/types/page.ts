import type { Language } from '$lib/common/domain/configurations'

export type LanguageAware = {
  lang: string
  language: Language
}
