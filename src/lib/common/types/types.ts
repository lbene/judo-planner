export type Option = {
  value: string
  label: string
}

export type SelectUpdateEvent = {
  update: Option
}
