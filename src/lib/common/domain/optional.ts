// See prelude-ts for more
export interface Optional<T> {
  map<ToOther>(mapperFn: (value: T) => ToOther): Optional<ToOther>
  flatMap<ToOther>(mapperFn: (value: T) => Optional<ToOther>): Optional<ToOther>
  orElse(other: Optional<T>): Optional<T>
  get(): T
  getOrElse(other: T): T
  getOrThrow(errorInfo?: Error | string): T
  isSome(): this is Some<T>
  isNone(): this is None<T>
}

export function optionalOf<T>(value: T | null | undefined): Optional<T> {
  return value === undefined || value === null ? new None<never>() : new Some(value)
}

class Some<T> implements Optional<T> {
  private value: T

  constructor(value: T) {
    this.value = value
  }

  map<ToOther>(mapperFn: (value: T) => ToOther): Optional<ToOther> {
    return optionalOf<ToOther>(mapperFn(this.value))
  }

  flatMap<ToOther>(mapperFn: (value: T) => Optional<ToOther>): Optional<ToOther> {
    return mapperFn(this.value)
  }

  orElse(other: Optional<T>): Optional<T> {
    return this
  }

  isSome(): this is Some<T> {
    return true
  }

  isNone(): this is None<T> {
    return false
  }

  getOrElse(other: T): T {
    return this.value
  }

  get() {
    return this.value
  }

  getOrThrow(errorInfo?: Error | string): T {
    return this.value
  }
}

class None<T> implements Optional<T> {
  map<ToOther>(mapperFn: (value: T) => ToOther): Optional<ToOther> {
    return new None<ToOther>()
  }

  getOrElse<T>(other: T): T {
    return other
  }

  flatMap<ToOther>(mapperFn: (value: T) => Optional<ToOther>): Optional<ToOther> {
    return new None<ToOther>()
  }

  orElse(other: Optional<T>): Optional<T> {
    return other
  }

  isSome(): this is Some<T> {
    return false
  }

  isNone(): this is None<T> {
    return true
  }

  getOrThrow(errorInfo?: Error | string): T {
    if (typeof errorInfo === 'string') {
      throw new Error(errorInfo || 'getOrThrow called on none!')
    }
    throw errorInfo || new Error('getOrThrow called on none!')
  }

  get(): T {
    throw new Error('get called on none!')
  }
}
