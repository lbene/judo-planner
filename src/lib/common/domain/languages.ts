import { defaultLanguage, supportedLanguages } from '$lib/common/domain/configurations'
import type { Language } from '$lib/common/domain/configurations'
import i18next from 'i18next'
import { writable } from 'svelte/store'
import { goto } from '$app/navigation'

export function findLanguageByCode(langCode: string): Language {
  const filtered = supportedLanguages.filter((language) => language.code === langCode)

  if (filtered.length === 0) {
    return defaultLanguage
  }

  return filtered[0]
}

export function changeLanguage(langCode: string) {
  const language = findLanguageByCode(langCode)

  i18next.changeLanguage(langCode).then(() => {
    currentLanguage.set(language)

    const splitUrl = document.location.pathname.split('/')
    const query = document.location.search.toString()

    splitUrl[1] = i18next.language

    const newUrl = splitUrl.join('/')

    goto(newUrl + query)
  })
}

export const currentLanguage = writable<Language>()
