import type { DaysByWeek } from '../../../routes/[lang]/lessons/_uiLogic/calendarV1/filters'
import type { Lesson } from '$lib/lessons/domain/lessons'

export type SearchParam = {
  name: string
  value: string
}

export function appendToSearchParams(url: URL, params: SearchParam[]): URL {
  const newUrl = new URL(url)

  const query = new URLSearchParams(newUrl.searchParams.toString())

  params.forEach((param) => {
    query.set(param.name, param.value)
  })

  newUrl.search = `?${query.toString()}`

  return newUrl
}

export function range(start: number, end: number) {
  return [...new Array(end + 1 - start)].fill(start).map((value, i) => i + value)
}

export function getMonths(languageCode: string): string[] {
  return [...Array(12).keys()].map((value) => getMonthName(value, languageCode))
}

export function getDayNumber(day: number) {
  const options: Intl.DateTimeFormatOptions = { day: '2-digit' }

  return new Intl.DateTimeFormat('en', options).format(new Date(0, 0, day))
}

export function formatDate(languageCode: string, date: Date): string {
  const dayName = new Intl.DateTimeFormat(languageCode, {
    weekday: 'long',
  }).format(date)

  const day = new Intl.DateTimeFormat(languageCode, {
    day: '2-digit',
  }).format(date)

  const month = new Intl.DateTimeFormat(languageCode, {
    month: 'short',
  }).format(date)

  return `${firstToUppercase(dayName)}, ${day} ${firstToUppercase(month)}`
}

export function formatDateStandard(date: Date): string {
  let month = date.getMonth() + 1
  const day = date.getDate()

  const monthString = month < 10 ? `0${month}` : month
  const dayString = day < 10 ? `0${day}` : day

  return `${date.getFullYear()}-${monthString}-${dayString}`
}

export function formatDateIso(date: Date): string {
  const formattedDate = formatDateStandard(date)

  const formattedTime = date.toTimeString().slice(0, 8)

  return `${formattedDate}T${formattedTime}`
}

export function getMonthName(month: number, languageCode: string): string {
  const options: Intl.DateTimeFormatOptions = { month: 'short' }

  return new Intl.DateTimeFormat(languageCode, options).format(new Date(0, month))
}

export function getNumberOfDays(year: number, month: number) {
  return new Date(year, month, 0).getDate()
}

export function getCurrentWeek(date: Date) {
  const week = Object.entries(getDaysByWeek(date.getFullYear().toString(), date.getMonth().toString())).find((value) => {
    const day = value[1].find((value) => value.date === date.getDate())

    return !!day
  })

  if (!week) {
    return 1
  }

  return week[0]
}

export function getDaysByWeek(year: string, month: string): DaysByWeek {
  const numberOfDays = getNumberOfDays(Number(year), Number(month) + 1)
  const options: Intl.DateTimeFormatOptions = { weekday: 'short' }

  let groupNumber = 1

  return [...Array(numberOfDays).keys()]
    .map<{ date: number; name: string }>((dayNumber) => ({
      date: dayNumber + 1,
      name: new Intl.DateTimeFormat(undefined, options).format(new Date(Number(year), Number(month), dayNumber + 1)),
    }))
    .reduce((daysGroupedByWeek, currentValue) => {
      if (currentValue.name === 'Mon') {
        ++groupNumber
      }

      if (!daysGroupedByWeek[groupNumber]) {
        daysGroupedByWeek[groupNumber] = [currentValue]
      } else {
        daysGroupedByWeek[groupNumber].push(currentValue)
      }

      return daysGroupedByWeek
    }, {} as DaysByWeek)
}

export function formatDuration(minute: number, second: number): string {
  const min = minute < 10 ? '0' + minute : minute

  const sec = second < 10 ? '0' + second : second

  return `${min}:${sec}`
}

export function formatSecondsDuration(seconds: number): string {
  const abs = Math.abs(seconds)

  const minute = Math.floor(abs / 60)

  const second = abs - minute * 60

  return seconds >= 0 ? formatDuration(minute, second) : '-' + formatDuration(minute, second)
}

export function lessonIsStrictlyInTheFuture(lesson: Lesson) {
  const now = new Date()
  now.setHours(0, 0, 0, 0)

  const lessonDate = lesson.date
  lessonDate.setHours(0, 0, 0, 0)

  return lessonDate.getTime() > now.getTime()
}

export function lessonIsNotInThePast(lesson: Lesson) {
  return dateIsInNotThePast(lesson.date)
}
2
export function dateIsInThePast(date: Date) {
  return !dateIsInNotThePast(date)
}

export function dateIsInNotThePast(date: Date) {
  const now = new Date()
  now.setHours(0, 0, 0, 0)

  date.setHours(0, 0, 0, 0)

  return date.getTime() >= now.getTime()
}

export function calculateInterval(date1: Date, date2: Date) {
  return Math.abs((date1.getTime() - date2.getTime()) / 1000)
}

export function transformWatchLinkToEmbedLink(watchLink: string) {
  return watchLink.replace('watch?v=', 'embed/')
}

export function pipe<T>(...fns: any[]) {
  return fns.reduce((carry, func) => {
    return func(carry)
  }, null)
}

export function isImage(string: string) {
  const regex: RegExp = /\.(jpeg|jpg|png|gif)/g

  return regex.test(string.toLowerCase())
}

function firstToUppercase(string: string) {
  return string[0].toUpperCase() + string.slice(1)
}
