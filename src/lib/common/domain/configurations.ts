import { range } from '$lib/common/domain/common'
import type { Option } from '$lib/common/types/types'
import type { LessonItemDuration } from '$lib/lessons/components/lessonItems/presentation'
import type { Lesson } from '$lib/lessons/domain/lessons'

export type Language = {
  code: string
  label: string
  isoCode: string
}

export const defaultLanguage: Language = {
  code: 'ro',
  label: 'RO',
  isoCode: 'ro-RO',
}

export const supportedLanguages = [
  defaultLanguage,
  {
    code: 'hu',
    label: 'HU',
    isoCode: 'hu-HU',
  },
  {
    code: 'en',
    label: 'EN',
    isoCode: 'en-EN',
  },
]

const currentYear = new Date().getFullYear()

export const years: Option[] = range(currentYear - 2, currentYear)
  .sort((a, b) => b - a)
  .map((value) => ({
    value: value + '',
    label: value + '',
  }))

export const chaptersAreNotStatic = false

export function shouldBeHistoryV2(lesson: Lesson) {
  return lesson.date > new Date('2023-09-13')
}

export const defaultLessonItemDuration: LessonItemDuration = {
  minute: 0,
  second: 0,
}
