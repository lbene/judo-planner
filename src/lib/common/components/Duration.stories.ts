import Duration from '$lib/common/components/Duration.svelte'

export default {
  title: 'common/Duration',
  component: Duration,
  tags: ['autodocs'],
}

export const LessThan1Minute = {
  args: {
    duration: 30,
  },
}

export const MoreThan1Minute = {
  args: {
    duration: 80,
  },
}
