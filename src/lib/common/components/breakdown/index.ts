export type BreakdownProps = LineItem[]

export type LineItem = {
  label: string
  value: string
}
