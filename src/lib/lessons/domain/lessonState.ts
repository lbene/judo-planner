import { writable } from 'svelte/store'
import type { Lesson } from '$lib/lessons/domain/lessons'
import type { Duration } from '$lib/lessons/domain/lessons'
import type { Chapter, LessonItem } from '$lib/lessons/domain/lessons'
import { getLessonItemDuration } from '$lib/lessons/domain/lesson'

export type LessonOperation =
  | 'initialized'
  | 'stop'
  | 'forceStop'
  | 'start'
  | 'retryLessonItem'
  | 'pause'
  | 'resume'
  | 'jumpTo'
  | 'completedLessonItem'
  | 'startedLessonItem'

export type EventPayload = {
  lesson: Lesson
  chapterId: string
  lessonItemId: string
  lessonItem: LessonItem
}

export type PlayerEvent = {
  operation: LessonOperation
  payload?: EventPayload
}

export type LessonStatus = {
  currentChapter?: Chapter
  currentLessonItem?: LessonItem
  currentLessonItemRemainingTime?: number
} & LessonPauseStatus

export type LessonPauseStatus = {
  inProgress: boolean
  isPaused: boolean
}

export const chapterNumberStore = writable<number>(0)
export const lessonItemNumberStore = writable<number>(0)
export const lessonItemRemainingTimeStore = writable<number>(Infinity)

export const lessonPauseStatus = writable<LessonPauseStatus>({
  inProgress: false,
  isPaused: false,
})

export function startLesson(lesson: Lesson) {
  lessonItemRemainingTimeStore.set(getLessonItemDuration(lesson.chapters[0].lessonItems[0].duration))

  lessonPauseStatus.update((status) => ({
    ...status,
    inProgress: true,
  }))
}

export function pauseLesson() {
  // fails when retry lesson item

  lessonPauseStatus.update((status) => ({
    ...status,
    isPaused: true,
  }))
}

export function resume() {
  lessonPauseStatus.update((status) => ({
    ...status,
    isPaused: false,
  }))
}

export function stopLesson() {
  lessonPauseStatus.update(() => ({
    inProgress: false,
    isPaused: false,
  }))

  chapterNumberStore.set(0)
  lessonItemNumberStore.set(0)
  lessonItemRemainingTimeStore.set(Infinity)
}

export function nextLessonItem(lesson: Lesson, chapterNumber: number, lessonItemNumber: number) {
  const nextLessonItemNumber = lessonItemNumber + 1

  if (lesson.chapters[chapterNumber].lessonItems.length === nextLessonItemNumber) {
    const newChapterNumber = nextChapter(lesson, chapterNumber)

    if (!lesson.chapters[newChapterNumber]) {
      return null
    }

    if (!lesson.chapters[newChapterNumber].lessonItems[0]) {
      return null
    }

    return {
      chapterId: lesson.chapters[newChapterNumber].id,
      nextLessonItemId: lesson.chapters[newChapterNumber].lessonItems[0].id,
    }
  }

  const nextLessonItem = lesson.chapters[chapterNumber].lessonItems[nextLessonItemNumber]

  const time = getLessonItemDuration(nextLessonItem.duration)

  lessonItemRemainingTimeStore.set(time)
  lessonItemNumberStore.update((value) => value + 1)

  return {
    chapterId: lesson.chapters[chapterNumber].id,
    nextLessonItemId: nextLessonItem.id,
  }
}

function nextChapter(lesson: Lesson, chapterNumber: number) {
  const nextChapterNumber = chapterNumber + 1

  const time =
    lesson.chapters[nextChapterNumber] && lesson.chapters[nextChapterNumber].lessonItems[0]
      ? getLessonItemDuration(lesson.chapters[nextChapterNumber].lessonItems[0].duration)
      : Infinity

  chapterNumberStore.update((value) => value + 1)
  lessonItemNumberStore.set(0)
  lessonItemRemainingTimeStore.set(time)

  return nextChapterNumber
}

export function countDownLessonItemTime() {
  lessonItemRemainingTimeStore.update((value) => value - 1)
}

export function resetLesson(duration: Duration) {
  lessonItemRemainingTimeStore.update(() => getLessonItemDuration(duration))

  lessonPauseStatus.update((status) => ({
    ...status,
    isPaused: false,
  }))
}

export function jumpTo(lesson: Lesson, chapterId: string, lessonItemId: string) {
  const chapter = lesson.chapters.find((chapter) => chapter.id === chapterId)

  if (!chapter) {
    console.error('The jumpTo event received a chapter ID that does not exist on the lesson', lesson)
    throw new Error('The chapter ID missing is: ' + chapterId)
  }

  const lessonItem = chapter.lessonItems.find((lessonItem) => lessonItem.id === lessonItemId)

  if (!lessonItem) {
    console.error('The jumpTo event received a lessonItem ID that does not exist on the lesson', lesson)
    throw new Error('The lessonItem ID missing is: ' + lessonItemId)
  }

  const chapterNumber = lesson.chapters.indexOf(chapter) ?? 0
  const lessonItemNumber = chapter.lessonItems.indexOf(lessonItem) ?? 0
  const time = getLessonItemDuration(lesson.chapters[chapterNumber].lessonItems[lessonItemNumber].duration)

  chapterNumberStore.update((value) => chapterNumber)
  lessonItemNumberStore.update((value) => lessonItemNumber)
  lessonItemRemainingTimeStore.set(time)

  lessonPauseStatus.update((status) => ({
    ...status,
    isPaused: false,
  }))
}
