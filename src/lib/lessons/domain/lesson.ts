import type { Chapter, Duration, Lesson, LessonItem } from '$lib/lessons/domain/lessons'
import { pipe } from '$lib/common/domain/common'

type Result = {
  ok: Function
  default: () => Duration
}

export function lessonIsNotEmpty(lesson: Lesson) {
  const lessonItemCount = lesson.chapters.reduce((count, chapter) => {
    count += chapter.lessonItems.length

    return count
  }, 0)

  return lessonItemCount !== 0
}

export function getLessonItem(lesson: Lesson, chapterId: string, lessonItemId: string): LessonItem {
  return pipe(isLesson(lesson), findChapter(chapterId), findLessonItem(lessonItemId))
}

function isLesson(lesson: Lesson) {
  return () => {
    return lesson
  }
}

export function findChapter(chapterId: string) {
  return (lesson: Lesson) => {
    return lesson.chapters.find((chapter) => chapter.id === chapterId)
  }
}

export function findLessonItem(lessonItemId: string) {
  return (chapter: Chapter | undefined) => {
    return chapter?.lessonItems.find((lessonItem) => lessonItem.id === lessonItemId)
  }
}

export type LessonBreakdown = {
  totalActiveTime: number
  totalPauseTime: number
  totalTime: number
}

export function calculateChapterDuration(lessonItems: LessonItem[]): number {
  return lessonItems.reduce((carry, item) => carry + getLessonItemDuration(item.duration), 0)
}

export function getLessonBreakdown(lesson: Lesson): LessonBreakdown {
  const initialBreakdown = lesson.chapters.reduce(
    (carry, chapter) => {
      chapter.lessonItems.forEach((lessonItem) => {
        if (lessonItem.title === 'Szünet') {
          carry.totalPauseTime = carry.totalPauseTime + getLessonItemDuration(lessonItem.duration)
        } else {
          carry.totalActiveTime = carry.totalActiveTime + getLessonItemDuration(lessonItem.duration)
        }
      })

      return carry
    },
    {
      totalActiveTime: 0,
      totalPauseTime: 0,
    }
  )

  return {
    ...initialBreakdown,
    totalTime: initialBreakdown.totalActiveTime + initialBreakdown.totalPauseTime,
  }
}

export function getLessonItemDuration(duration: Omit<Duration, 'formatted'>) {
  return duration.minute * 60 + duration.second
}

export function isSoundEnabled(lessonItem: LessonItem) {
  return !!lessonItem.soundEffect
}
