import { isImage } from '$lib/common/domain/common'
import { loadTechniques } from '$lib/lessons/storage/techniquesLocalStorage'

export const techniquesStorageId = 'techniques'

export type Technique = {
  id: string
  name: string
  video?: string
}

export function getTechniqueNames(techniques: Technique[]): Record<string, string> {
  return techniques.reduce((carry, technique) => {
    carry[technique.id] = technique.name

    return carry
  }, {} as Record<string, string>)
}

export function hasVideo(technique: Technique) {
  return technique.video && technique.video.includes('youtube')
}

export function hasImage(technique: Technique) {
  return technique.video && isImage(technique.video)
}

export function addNewTechnique(name: string, video?: string): Technique[] {
  const rawTechniques = localStorage.getItem(techniquesStorageId)

  if (rawTechniques === null) {
    const list = [createTechnique('Szünet'), createTechnique(name, video)]

    localStorage.setItem(techniquesStorageId, JSON.stringify(list))

    return list
  }

  const list: Technique[] = JSON.parse(rawTechniques)

  list.push(createTechnique(name, video))

  localStorage.setItem(techniquesStorageId, JSON.stringify(list))

  return list
}

export function deleteTechnique(id: string): Technique[] {
  const list = loadTechniques().filter((value) => value.id !== id)

  localStorage.setItem(techniquesStorageId, JSON.stringify(list))

  return list
}

export function editTechnique(id: string, name: string, video?: string): Technique[] {
  const list = loadTechniques().map((value) => {
    if (value.id === id) {
      return {
        ...value,
        name,
        video,
      }
    }

    return value
  })

  localStorage.setItem(techniquesStorageId, JSON.stringify(list))

  return list
}

function createTechnique(name: string, video?: string): Technique {
  return {
    id: crypto.randomUUID(),
    name,
    video,
  }
}
