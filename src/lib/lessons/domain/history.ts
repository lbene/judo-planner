import type { Lesson } from '$lib/lessons/domain/lessons'
import type { LessonOperation, PlayerEvent } from '$lib/lessons/domain/lessonState'
import { formatDateIso, formatDateStandard } from '$lib/common/domain/common'

type PersistedPlayerEvent = {
  event: PlayerEvent
  createdAt: Date
}

export type NoteId = string

export type Note = {
  readonly id: NoteId
  content: string
  readonly createdAt: Date
  updatedAt: Date
}

export type LessonHistory = {
  lessonId: string
  startedAt: Date
  pausedTime: number
  stoppedAt?: Date
  events: PersistedPlayerEvent[]
  notes?: Note[]
}

type Status = LessonOperation | 'skipped' | 'incomplete'

export type LessonItemStatus = {
  lessonItemId: string
  status: Status
}

export type ProcessedHistory = {
  totalTime: number
  totalActiveTime: number
  totalPauseTime: number
  lessonItemStatuses: Record<string, LessonItemStatus>
  lessonId: string
}

export function createHistory(lesson: Lesson, event: PlayerEvent) {
  const localStorageId = `lesson_history_${lesson.id}`
  const now = new Date()

  const entry: LessonHistory = {
    lessonId: lesson.id,
    startedAt: now,
    pausedTime: 0,
    events: [
      {
        event,
        createdAt: now,
      },
    ],
    notes: [],
  }

  localStorage.setItem(localStorageId, serialize(entry))
}

export function update(history: LessonHistory, events: PersistedPlayerEvent[]) {
  const localStorageId = `lesson_history_${history.lessonId}`

  const updated: LessonHistory = {
    ...history,
    events: [...history.events, ...events],
  }

  localStorage.setItem(localStorageId, serialize(updated))
}

export function loadHistory(lesson: Lesson): LessonHistory | undefined {
  const localStorageId = `lesson_history_${lesson.id}`

  const data = localStorage.getItem(localStorageId)

  if (data) {
    return deserialize(data)
  }
}

/**
 * @deprecated all note related functionality
 */
export function loadNotes(lessonId: string) {
  const localStorageId = `lesson_history_${lessonId}`

  const data = localStorage.getItem(localStorageId)

  if (!data) {
    throw new Error('Could not find history for lesson id: ' + lessonId)
  }

  return deserialize(data).notes
}

export function addNoteToHistory(lessonId: string, noteContent: string) {
  const localStorageId = `lesson_history_${lessonId}`

  const data = localStorage.getItem(localStorageId)

  if (!data) {
    throw new Error('Could not find history for lesson id: ' + lessonId)
  }

  const history = deserialize(data)

  const note: Note = {
    id: crypto.randomUUID(),
    content: noteContent,
    createdAt: new Date(),
    updatedAt: new Date(),
  }

  if (!history.notes) {
    history.notes = [note]
  } else {
    history.notes = [...history.notes, ...[note]]
  }

  localStorage.setItem(localStorageId, serialize(history))

  return history.notes
}

export function updateNote(lessonId: string, noteId: NoteId, noteContent: string) {
  const localStorageId = `lesson_history_${lessonId}`

  const data = localStorage.getItem(localStorageId)

  if (!data) {
    throw new Error('Could not find history for lesson id: ' + lessonId)
  }

  const history = deserialize(data)

  if (!history.notes) {
    throw new Error('Could not find notes for history with lesson id: ' + lessonId)
  }

  const notes = history.notes.map((note) => {
    if (note.id === noteId) {
      return {
        ...note,
        updatedAt: new Date(),
        content: noteContent,
      }
    }

    return note
  })

  history.notes = [...notes]

  localStorage.setItem(localStorageId, serialize(history))

  return history.notes
}

export function deleteNote(lessonId: string, noteId: NoteId) {
  const localStorageId = `lesson_history_${lessonId}`

  const data = localStorage.getItem(localStorageId)

  if (!data) {
    throw new Error('Could not find history for lesson id: ' + lessonId)
  }

  const history = deserialize(data)

  if (!history.notes) {
    throw new Error('Could not find notes for history with lesson id: ' + lessonId)
  }

  const notes = history.notes.filter((note) => note.id !== noteId)

  history.notes = [...notes]

  localStorage.setItem(localStorageId, serialize(history))

  return history.notes
}

export function deleteExistingHistory(lesson: Lesson) {
  const localStorageId = `lesson_history_${lesson.id}`

  localStorage.removeItem(localStorageId)
}

export function setStatusForLessonItem(lessonItemId: string, status: Status): LessonItemStatus {
  const processedStatus = ['pause', 'retryLessonItem', 'startedLessonItem', 'start', 'jumpTo'].includes(status)
    ? 'incomplete'
    : status

  return {
    lessonItemId,
    status: processedStatus,
  }
}

export function setNewStatusForLessonItem(lessonItemIdStatus: LessonItemStatus, status: Status): LessonItemStatus {
  // jumping in the same chapter marks the lesson completed
  if (['completedLessonItem'].includes(lessonItemIdStatus.status)) {
    return {
      ...lessonItemIdStatus,
      status: 'completedLessonItem',
    }
  }

  if (['stop'].includes(status)) {
    return {
      ...lessonItemIdStatus,
      status: 'completedLessonItem',
    }
  }

  if (
    ['pause', 'retryLessonItem', 'startedLessonItem', 'start', 'jumpTo', 'forceStop'].includes(lessonItemIdStatus.status)
  ) {
    return {
      ...lessonItemIdStatus,
      status: 'incomplete',
    }
  }

  // We should segregate statuses from player events, statuses should be derived events
  if (['pause', 'retryLessonItem', 'startedLessonItem', 'start', 'jumpTo', 'forceStop'].includes(status)) {
    return {
      ...lessonItemIdStatus,
      status: 'incomplete',
    }
  }

  return {
    ...lessonItemIdStatus,
    status,
  }
}

function serialize(events: LessonHistory) {
  return JSON.stringify(events, function (key, value) {
    if (this[key] instanceof Date) {
      const date = this[key]

      try {
        return formatDateIso(date)
      } catch (error) {
        console.log(error)
        console.error('Could not format date with key: ' + key)
        console.error('Could not format date', date)
        console.error('Standard format would be' + formatDateStandard(date))
      }
    }

    return value
  })
}

function deserialize(value: string): LessonHistory {
  return JSON.parse(value, (key, value) => {
    const dateKeys = ['createdAt', 'startedAt', 'stoppedAt', 'updatedAt']

    if (dateKeys.includes(key)) {
      return new Date(Date.parse(value))
    }

    return value
  })
}
