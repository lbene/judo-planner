import type { Note } from '$lib/lessons/domain/history'

export enum StatusValues {
  NOT_STARTED = -1,
  STARTED = 0,
  FINISHED = 1,
}

export type PlayableLessonItemId = string

export type LessonHistory = {
  lessonId: string
  startedAt: Date
  statuses: Record<PlayableLessonItemId, StatusValues>
  notes: Note[]
}
