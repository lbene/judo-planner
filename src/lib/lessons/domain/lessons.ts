import { formatDateStandard, getCurrentWeek } from '$lib/common/domain/common'
import { writable } from 'svelte/store'
import type { TemporaryLesson } from '$lib/lessons/components/lessonNavigation/presentation'

export type Duration = {
  minute: number
  second: number
  formatted: string
}

type EntityBase = {
  readonly id: string
  title: string
}

export type LessonSummary = EntityBase & {
  date: Date
}

export type Lesson = EntityBase &
  LessonSummary & {
    chapters: Chapter[]
    readonly createdAt: Date
  }

export type Chapter = EntityBase & {
  lessonItems: LessonItem[]
}

export type LessonItem = EntityBase & {
  duration: Duration
  techniqueId: string
  soundEffect?: boolean
}

/**
 * @deprecated
 */
export function loadLessonsOfTheWeek(year: string, month: string, week: string): Record<number, Lesson> | null {
  const lessonsPerWeek = localStorage.getItem(`year:${year}-month:${month}-week:${week}`)

  if (!lessonsPerWeek) {
    return null
  }

  const lessons = deserialize(lessonsPerWeek) as Lesson[]

  return listOfLessonsToLessonsPerWeek(lessons)
}

export function saveNewLesson(date: Date): Record<number, Lesson> {
  const chapters = ['setup', 'preparation', 'fundamentals', 'end'].map((title) => {
    return {
      id: crypto.randomUUID(),
      title,
      lessonItems: [],
    }
  })

  const lesson: Lesson = {
    title: 'Titlu',
    chapters: chapters,
    createdAt: new Date(),
    date,
    id: crypto.randomUUID(),
  }

  saveLesson(lesson)

  const formattedDate = formatDateStandard(lesson.date)

  addToLessonDatesIndex({
    [formattedDate]: lesson.id,
  })
  return loadLessonsPerWeekFromLesson(lesson) as Record<number, Lesson>
}

export function duplicateLesson(originalLesson: Lesson, newDate: Date): Record<number, Lesson> {
  const lesson: Lesson = {
    ...originalLesson,
    id: crypto.randomUUID(),
    date: newDate,
  }

  saveLesson(lesson)

  const formattedDate = formatDateStandard(lesson.date)

  addToLessonDatesIndex({
    [formattedDate]: lesson.id,
  })

  return loadLessonsPerWeekFromLesson(originalLesson) as Record<number, Lesson>
}

export function updateLesson(lesson: Lesson, originalLesson?: Lesson) {
  // TODO consider separate indexing worker for the grouping in weeks or views
  const id = getLocalStorageIdFromLesson(lesson)
  const lessonsPerWeek = localStorage.getItem(id)

  if (originalLesson && getLocalStorageIdFromLesson(originalLesson) !== id) {
    const originalId = getLocalStorageIdFromLesson(originalLesson)
    const originalLessonsPerWeek = localStorage.getItem(originalId)

    if (!originalLessonsPerWeek) {
      throw new Error('There should be lessons for this week, because you are editing: ' + serialize(originalLesson))
    } else {
      const lessons: Lesson[] = deserialize(originalLessonsPerWeek)

      const existingLessonIndex = lessons.findIndex((value) => value.id === lesson.id)

      if (existingLessonIndex === -1) {
        throw new Error('This should exist, because you are editing: ' + serialize(lesson))
      } else {
        saveLesson(lesson)

        lessons.splice(existingLessonIndex, 1)

        localStorage.setItem(originalId, serialize(lessons))

        // Because we changed the date on the lesson, we need to remove the original date from the index.
        const originalDate = formatDateStandard(originalLesson.date)
        deleteFromLessonDatesIndex(originalDate)

        const date = formatDateStandard(lesson.date)

        addToLessonDatesIndex({
          [date]: lesson.id,
        })
      }
    }

    return
  }

  if (!lessonsPerWeek) {
    throw new Error('There should be lessons for this week, because you are editing: ' + serialize(lesson))
  } else {
    const lessons: Lesson[] = deserialize(lessonsPerWeek)

    const existingLessonIndex = lessons.findIndex((value) => value.id === lesson.id)

    if (existingLessonIndex === -1) {
      throw new Error('This should exist, because you are editing: ' + serialize(lesson))
    } else {
      lessons[existingLessonIndex] = lesson

      localStorage.setItem(id, serialize(lessons))
    }
  }

  if (originalLesson) {
    // Because we changed the date on the lesson, we need to remove the original date from the index.
    const originalDate = formatDateStandard(originalLesson.date)
    deleteFromLessonDatesIndex(originalDate)
  }

  const date = formatDateStandard(lesson.date)

  addToLessonDatesIndex({
    [date]: lesson.id,
  })
}

export function deleteLesson(lesson: Lesson) {
  const id = getLocalStorageIdFromLesson(lesson)

  const lessonsPerWeek = localStorage.getItem(id)

  if (lessonsPerWeek) {
    const lessons: Lesson[] = deserialize(lessonsPerWeek)

    const newLessons = lessons.filter((value) => value.id !== lesson.id)

    localStorage.setItem(id, serialize(newLessons))
  }

  deleteFromLessonDatesIndex(formatDateStandard(lesson.date))

  return loadLessonsPerWeekFromLesson(lesson)
}

export function getLessonDatesIndex(): Record<string, string> {
  let dateIndex = localStorage.getItem('index_lesson_dates')

  if (dateIndex === null) {
    dateIndex = '{}'
    localStorage.setItem('index_lesson_dates', dateIndex)
  }

  return JSON.parse(dateIndex)
}

export function addNewChapter(title: string, lesson: Lesson): Lesson {
  const chapter: Chapter = {
    id: crypto.randomUUID(),
    title,
    lessonItems: [],
  }

  const newLesson: Lesson = {
    ...lesson,
    chapters: [...lesson.chapters, ...[chapter]],
  }

  updateLesson(newLesson)

  return newLesson
}

export function editChapterTitle(title: string, chapterId: string, lesson: Lesson): Lesson {
  const chapters = lesson.chapters.map((value) => {
    if (chapterId === value.id) {
      return {
        ...value,
        title,
      }
    }
    return value
  })

  const newLesson: Lesson = {
    ...lesson,
    chapters,
  }

  updateLesson(newLesson)

  return newLesson
}

export function deleteChapter(chapterId: string, lesson: Lesson): Lesson {
  const chapters = lesson.chapters.filter((value) => value.id !== chapterId)

  const newLesson: Lesson = {
    ...lesson,
    chapters,
  }

  updateLesson(newLesson)

  return newLesson
}

export function moveChapterUp(chapterId: string, lesson: Lesson): Lesson {
  return swapChapters(chapterId, lesson, (chapters, chapterPosition) => {
    const newChapters = [...chapters]

    const tmp = newChapters[chapterPosition]
    newChapters[chapterPosition] = newChapters[chapterPosition - 1]
    newChapters[chapterPosition - 1] = tmp

    return newChapters
  })
}

export function moveChapterDown(chapterId: string, lesson: Lesson): Lesson {
  return swapChapters(chapterId, lesson, (chapters, chapterPosition) => {
    const newChapters = [...chapters]

    const tmp = newChapters[chapterPosition]
    newChapters[chapterPosition] = newChapters[chapterPosition + 1]
    newChapters[chapterPosition + 1] = tmp

    return newChapters
  })
}

export function addNewLessonItems(items: TemporaryLesson[], chapterId: string, lesson: Lesson): Lesson {
  const lessonItems = items.map<LessonItem>((value) => {
    return {
      id: crypto.randomUUID(),
      title: value.name,
      duration: {
        minute: value.minute,
        second: value.second,
        formatted: value.duration,
      },
      techniqueId: value.id,
      soundEffect: false,
    }
  })

  const chapter = lesson.chapters.find((value) => value.id === chapterId)

  if (!chapter) {
    console.error('Could not find chapter in lesson when adding new lesson items', items, chapterId, lesson)
    return lesson
  }

  const newItems = [...chapter.lessonItems, ...lessonItems]

  return saveLessonItems(newItems, chapterId, lesson)
}

export function removeLessonItem(lessonItemId: string, chapterId: string, lesson: Lesson): Lesson {
  const chapter = lesson.chapters.find((value) => value.id === chapterId)

  if (!chapter) {
    console.error('Could not find chapter in lesson when removing lesson item', lessonItemId, chapterId, lesson)
    return lesson
  }

  const lessonItems = chapter.lessonItems.filter((value) => value.id !== lessonItemId)

  return saveLessonItems(lessonItems, chapterId, lesson)
}

export function cloneLessonItem(lessonItemId: string, chapterId: string, lesson: Lesson): Lesson {
  const chapter = lesson.chapters.find((value) => value.id === chapterId)

  if (!chapter) {
    console.error('Could not find chapter in lesson when removing lesson item', lessonItemId, chapterId, lesson)
    return lesson
  }

  const lessonItem = chapter.lessonItems.find((value) => value.id === lessonItemId)

  if (!lessonItem) {
    console.error('Could not find lessonItem in lesson when cloning', lessonItemId, chapterId, lesson)
    return lesson
  }

  const lessonItems = [
    ...chapter.lessonItems,
    {
      ...lessonItem,
      id: crypto.randomUUID(),
    },
  ]

  return saveLessonItems(lessonItems, chapterId, lesson)
}

export function updateChapterWithSortedLessonItems(
  lessonItems: LessonItem[],
  chapterId: string,
  lesson: Lesson
): Lesson {
  const chapter = lesson.chapters.find((value) => value.id === chapterId)

  if (!chapter) {
    console.error('Could not find chapter in lesson when re-ordering', chapterId, lesson)
    return lesson
  }

  return saveLessonItems(lessonItems, chapterId, lesson)
}

export function updateLessonItemDuration(
  duration: Duration,
  lessonItemId: string,
  chapterId: string,
  lesson: Lesson
): Lesson {
  const chapter = lesson.chapters.find((value) => value.id === chapterId)

  if (!chapter) {
    console.error('Could not find chapter in lesson when updating duration', lessonItemId, chapterId, lesson)
    return lesson
  }

  const lessonItem = chapter.lessonItems.find((value) => value.id === lessonItemId)

  if (!lessonItem) {
    console.error('Could not find lesson item in lesson when updating duration', lessonItemId, chapterId, lesson)
    return lesson
  }

  const lessonItems = chapter.lessonItems.map((value) => {
    if (value.id !== lessonItemId) {
      return value
    }

    return {
      ...value,
      duration,
    }
  })

  return saveLessonItems(lessonItems, chapterId, lesson)
}

export function setSoundEffectStatus(
  soundEffectStatus: boolean,
  lessonItemId: string,
  chapterId: string,
  lesson: Lesson
): Lesson {
  const chapter = lesson.chapters.find((value) => value.id === chapterId)

  if (!chapter) {
    console.error('Could not find chapter in lesson when updating sound effect status', lessonItemId, chapterId, lesson)
    return lesson
  }

  const lessonItems = chapter.lessonItems.map((lessonItem) => {
    if (lessonItem.id === lessonItemId) {
      return {
        ...lessonItem,
        soundEffect: soundEffectStatus,
      }
    }

    return lessonItem
  })

  return saveLessonItems(lessonItems, chapterId, lesson)
}

function saveLessonItems(lessonItems: LessonItem[], chapterId: string, lesson: Lesson) {
  const chapters = lesson.chapters.map((value) => {
    if (value.id !== chapterId) {
      return value
    }

    value.lessonItems = [...lessonItems]

    return value
  })

  const newLesson: Lesson = {
    ...lesson,
    chapters,
  }

  updateLesson(newLesson)

  return newLesson
}

function swapChapters(
  chapterId: string,
  lesson: Lesson,
  swapFunction: (chapters: Chapter[], chapterPosition: number) => Chapter[]
) {
  const chapterPosition = lesson.chapters.findIndex((value) => value.id === chapterId)

  const chapters = swapFunction(lesson.chapters, chapterPosition)

  const newLesson: Lesson = {
    ...lesson,
    chapters,
  }

  updateLesson(newLesson)

  return newLesson
}

function loadLessonsPerWeekFromLesson(lesson: Lesson): Record<string, Lesson> | null {
  const id = getLocalStorageIdFromLesson(lesson)

  const lessonsPerWeek = localStorage.getItem(id)!

  const lessons = deserialize(lessonsPerWeek) as Lesson[]

  return listOfLessonsToLessonsPerWeek(lessons)
}

function listOfLessonsToLessonsPerWeek(lessons: Lesson[]): Record<string, Lesson> | null {
  return lessons.reduce((acc: Record<string, Lesson> | null, lesson) => {
    if (acc === null) {
      const key = lesson.date.getDate()
      acc = {
        [key]: lesson,
      }

      return acc
    }

    const key = lesson.date.getDate()
    acc[key] = lesson

    return acc
  }, null)
}

function addToLessonDatesIndex(indexEntry: Record<string, string>) {
  const index = getLessonDatesIndex()

  updateLessonDatesIndex({
    ...index,
    ...indexEntry,
  })
}

function deleteFromLessonDatesIndex(key: string) {
  const index = getLessonDatesIndex()

  delete index[key]

  updateLessonDatesIndex(index)
}

function updateLessonDatesIndex(updatedIndex: Record<string, string>) {
  const dateIndex = JSON.stringify(updatedIndex)

  localStorage.setItem('index_lesson_dates', dateIndex)
}

function saveLesson(lesson: Lesson) {
  const id = getLocalStorageIdFromLesson(lesson)

  const lessonsPerWeek = localStorage.getItem(id)

  if (!lessonsPerWeek) {
    localStorage.setItem(id, serialize([lesson]))
  } else {
    const lessons: Lesson[] = deserialize(lessonsPerWeek)

    const existingLessonIndex = lessons.findIndex((value) => value.id === lesson.id)

    if (existingLessonIndex === -1) {
      lessons.push(lesson)

      localStorage.setItem(id, serialize(lessons))
    } else {
      throw new Error('You should not be here, this is just for adding')
    }
  }
}

function serialize(lesson: Lesson | Lesson[]) {
  return JSON.stringify(lesson, function (key, value) {
    if (this[key] instanceof Date) {
      const date = this[key]
      return formatDateStandard(date)
    }

    return value
  })
}

function deserialize(value: string) {
  return JSON.parse(value, (key, value) => {
    if (key === 'createdAt' || key === 'date') {
      return new Date(Date.parse(value))
    }
    return value
  })
}

function getLocalStorageIdFromLesson(
  lesson: EntityBase & { date: Date } & { chapters: Chapter[]; readonly createdAt: Date }
) {
  return `year:${lesson.date.getFullYear()}-month:${lesson.date.getMonth() + 1}-week:${getCurrentWeek(lesson.date)}`
}

export const activeLesson = writable<Lesson | null>()
