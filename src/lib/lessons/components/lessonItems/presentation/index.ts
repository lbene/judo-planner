export type LessonItemDuration = {
  minute: number
  second: number
}

export type LessonItemEvent = {
  lessonItemAdded: LessonItemEventPayload
  change: LessonItemEventPayload
}

export type LessonItemEventPayload = {
  duration: LessonItemDuration
  name: string
  id: string
}
