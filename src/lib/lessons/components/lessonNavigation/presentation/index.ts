export type AddLessonEvent = {
  click: AddLessonEventPayload
}

export type AddLessonEventPayload = {
  day: string
}

export type BaseLessonEventPayload = {
  day: string
}

export type LessonEvent = {
  editTitle: BaseLessonEventPayload
  editDate: BaseLessonEventPayload
  select: BaseLessonEventPayload
  delete: BaseLessonEventPayload
  clone: BaseLessonEventPayload
}

export type ElementEvent = {
  lessonElementEdit: ElementEventPayload
  lessonElementRemove: ElementEventPayload
  lessonElementClone: ElementEventPayload
  enableSound: ElementEventPayload
  disableSound: ElementEventPayload
  selectLessonElement: ElementEventPayload
  showVideo: ElementEventPayload
  showImage: ElementEventPayload
}

export type ChapterEvent = {
  lessonElementAdd: ChapterEventPayload
  chapterTitleEdit: ChapterEventPayload
  chapterDelete: ChapterEventPayload
  moveChapterUp: ChapterEventPayload
  moveChapterDown: ChapterEventPayload
}

export type ElementEventPayload = {
  elementId: string
  chapterId: string
}

export type ChapterEventPayload = {
  chapterId: string
}

export type AddChapterEventPayload = {
  title: string
}

export type AddChapterEvent = {
  submit: AddChapterEventPayload
}

export type EditTitleEventPayload = {
  title: string
}

export type EditTitleEvent = {
  submit: EditTitleEventPayload
}

export type DeleteEventPayload = {
  answer: boolean
}

export type DeleteEvent = {
  choice: DeleteEventPayload
}

export type TemporaryLesson = {
  id: string
  name: string
  minute: number
  second: number
  duration: string
}

export type TemporaryItemsEvent = {
  remove: TemporaryItemsEventPayload
  submit: TemporaryItemsEventPayload
}

export type TemporaryItemsEventPayload = {
  items: TemporaryLesson[]
}

export type EditLessonDurationEvent = {
  submit: EditLessonDurationEventPayload
}

export type EditLessonDurationEventPayload = {
  minute: number
  second: number
}

export type SubmitDateEvent = {
  submit: SubmitDateEventPayload
}

export type SubmitDateEventPayload = {
  date: Date
}

export type RemoveTemporaryItemEvent = {
  remove: RemoveTemporaryItemEventPayload
}

export type RemoveTemporaryItemEventPayload = {
  index: number
}
