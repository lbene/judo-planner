import type { NoteId } from '$lib/lessons/domain/history'

export type NoteEvent = {
  edit: NoteEventPayload
  delete: NoteEventPayload
}

export type NoteEventPayload = {
  noteId: NoteId
}
