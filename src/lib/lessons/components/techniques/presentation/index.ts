export type TechniqueEvent = {
  edit: TechniqueEventPayload
  delete: TechniqueEventPayload
}

export type TechniqueEventPayload = {
  techniqueId: string
}

export type SaveTechniqueEvent = {
  submit: SaveTechniqueEventPayload
}

export type SaveTechniqueEventPayload = {
  name: string
  video?: string
}

export type CircuitEvent = {
  edit: CircuitEventPayload
  delete: CircuitEventPayload
}

export type CircuitEventPayload = {
  id: string
}
