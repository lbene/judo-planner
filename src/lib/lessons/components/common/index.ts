export type PlayableLessonElementClickEvent = {
  click: PlayableLessonElementClickEventPayload
}

export type PlayableLessonElementClickEventPayload = {
  id: string
}
