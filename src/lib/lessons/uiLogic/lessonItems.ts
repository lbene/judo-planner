import type { Lesson } from '$lib/lessons/domain/lessons'
import { hasImage, hasVideo } from '$lib/lessons/domain/techniques'
import type { Technique } from '$lib/lessons/domain/techniques'

export function isLast(lessonItemId: string, chapterId: string, lesson?: Lesson) {
  if (!lesson) {
    return false
  }

  const chapterNumber = lesson.chapters.findIndex((chapter) => chapter.id === chapterId)

  if (chapterNumber < 0) {
    return false
  }

  const length = lesson.chapters[chapterNumber].lessonItems.length

  const lessonItemNumber = lesson.chapters[chapterNumber].lessonItems.findIndex((value) => value.id === lessonItemId)

  return lessonItemNumber === length - 1
}

export function showVideoIcon(techniques: Technique[], techniqueId: string) {
  const technique = techniques.find((value) => value.id === techniqueId)

  if (!technique) {
    return false
  }

  return hasVideo(technique)
}

export function showImageIcon(techniques: Technique[], techniqueId: string) {
  const technique = techniques.find((value) => value.id === techniqueId)

  if (!technique) {
    return false
  }

  return hasImage(technique)
}

export function getMediaLink(techniques: Technique[], techniqueId: string) {
  return techniques.find((value) => value.id === techniqueId)?.video
}
