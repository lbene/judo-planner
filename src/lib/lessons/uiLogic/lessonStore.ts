import type { Readable, Writable } from 'svelte/store'
import { derived, writable } from 'svelte/store'
import type { Chapter, Lesson } from '$lib/lessons/domain/lessons'
import {
  chapterNumberStore,
  countDownLessonItemTime,
  jumpTo,
  lessonItemNumberStore,
  lessonItemRemainingTimeStore,
  lessonPauseStatus,
  type LessonStatus,
  nextLessonItem,
  pauseLesson,
  type PlayerEvent,
  resetLesson,
  resume,
  startLesson,
  stopLesson,
} from '$lib/lessons/domain/lessonState'

import {
  createHistory,
  loadHistory,
  setNewStatusForLessonItem,
  setStatusForLessonItem,
  update,
} from '$lib/lessons/domain/history'

import { loadHistory as loadHistoryV2 } from '$lib/lessons/storage/historyLocalStorage'
import type { LessonItemStatus, ProcessedHistory } from '$lib/lessons/domain/history'
import { calculateInterval, lessonIsNotInThePast } from '$lib/common/domain/common'
import { getLessonItemDuration, isSoundEnabled } from '$lib/lessons/domain/lesson'
import { stopAudio } from '$lib/common/uiLogic/commonUi'
import sound from '$lib/assets/sound.mp3'
import lessonEnd from '$lib/assets/lessonEnd.mp3'
import { loadLessonConfig } from '$lib/config/storage/configLocalStorage'
import { shouldBeHistoryV2 } from '$lib/common/domain/configurations'

export const lessonsPerWeek = writable<Record<number, Lesson> | null>(null)

export const currentLesson: Writable<Lesson | undefined> = writable<Lesson | undefined>()

const status: LessonStatus = {
  inProgress: false,
  isPaused: false,
}

// TODO use import here so that we can leverage vite's hashing of the resource
const countdown = new Audio(sound)
const finalCountdown = new Audio(lessonEnd)

export const lessonStatus: Readable<LessonStatus> = derived(
  [chapterNumberStore, lessonItemNumberStore, currentLesson, lessonPauseStatus, lessonItemRemainingTimeStore],
  ([chapterNumber, lessonItemNumber, lesson, pauseStatus, currentLessonItemRemainingTime], set) => {
    if (!lesson) {
      return
    }

    if (lesson.chapters.length === chapterNumber) {
      playerEvents.update((value) => ({
        ...value,
        operation: 'stop',
      }))

      return
    }

    if (
      currentLessonItemRemainingTime === 6 &&
      isSoundEnabled(lesson.chapters[chapterNumber].lessonItems[lessonItemNumber])
    ) {
      countdown.play()
    }

    if (currentLessonItemRemainingTime === 1) {
      finalCountdown.play()
    }

    if (currentLessonItemRemainingTime === 0) {
      const nextLessonIt = nextLessonItem(lesson, chapterNumber, lessonItemNumber)

      if (!nextLessonIt) {
        playerEvents.update((value) => ({
          ...value,
          operation: 'stop',
        }))

        return
      }

      playerEvents.update((value) => {
        return {
          ...value,
          operation: 'completedLessonItem',
        }
      })

      // TODO this should be derived event
      playerEvents.update((value) => {
        return {
          ...value,
          operation: 'startedLessonItem',
          payload: {
            ...value.payload!,
            lessonItemId: nextLessonIt.nextLessonItemId,
            chapterId: nextLessonIt.chapterId,
          },
        }
      })

      return
    }

    const lessonItem = lesson.chapters[chapterNumber].lessonItems[lessonItemNumber]

    set({
      ...pauseStatus,
      currentChapter: lesson.chapters[chapterNumber],
      currentLessonItem: lessonItem,
      currentLessonItemRemainingTime: currentLessonItemRemainingTime,
    })
  },
  status
)

export const playerEvents = writable<PlayerEvent>({
  operation: 'initialized',
})

let lessonItemTimer: NodeJS.Timer | undefined

playerEvents.subscribe((event) => {
  if (event.operation === 'start') {
    if (!event.payload) {
      throw new Error('The start event must contain a payload')
    }

    startLesson(event.payload.lesson)

    lessonItemTimer = setInterval(countDownLessonItemTime, 1000)
  }

  if (event.operation === 'pause') {
    clearInterval(lessonItemTimer)

    pauseLesson()
  }

  if (event.operation === 'resume') {
    resume()

    lessonItemTimer = setInterval(countDownLessonItemTime, 1000)
  }

  if (event.operation === 'retryLessonItem') {
    clearInterval(lessonItemTimer)

    // TODO fix the typing here
    resetLesson(event.payload!.lessonItem!.duration)

    lessonItemTimer = setInterval(countDownLessonItemTime, 1000)
    stopAllAudio()
  }

  if (event.operation === 'jumpTo') {
    clearInterval(lessonItemTimer)

    const payload = event.payload!

    jumpTo(payload.lesson, payload.chapterId!, payload.lessonItemId!)

    lessonItemTimer = setInterval(countDownLessonItemTime, 1000)
    stopAllAudio()
  }

  if (['initialized', 'stop', 'forceStop'].includes(event.operation)) {
    clearInterval(lessonItemTimer)

    stopAllAudio()
    stopLesson()
  }

  if (event.operation !== 'initialized') {
    persistPlayerEvent(event.payload!.lesson, event)
  }
})

let globalElapsedTimer: NodeJS.Timer | undefined

export const globalElapsed = derived(
  playerEvents,
  (event, set) => {
    if (event.operation === 'start') {
      let elapsed = 0
      globalElapsedTimer = setInterval(() => {
        elapsed += 1

        set(elapsed)
      }, 1000)

      set(elapsed)
    }

    if (['stop', 'forceStop'].includes(event.operation)) {
      clearInterval(globalElapsedTimer)

      set(0)
    }
  },
  0
)

let globalRemainingTimer: NodeJS.Timer | undefined

export const globalRemaining: Readable<number> = derived(
  playerEvents,
  (event, set) => {
    if (event.operation === 'start') {
      const payload = event.payload!

      const elapsedPerChapter = (chapter: Chapter) =>
        chapter.lessonItems.reduce((carry, lessonItem) => carry + getLessonItemDuration(lessonItem.duration), 0)

      let elapsed = payload.lesson.chapters.reduce((carry, chapter) => carry + elapsedPerChapter(chapter), 0)

      globalRemainingTimer = setInterval(() => {
        --elapsed

        set(elapsed)
      }, 1000)

      set(elapsed)
    }

    if (['stop', 'forceStop'].includes(event.operation)) {
      clearInterval(globalRemainingTimer)

      set(0)
    }
  },
  0
)

export function shouldEnableEditing(lessonStatus: LessonStatus, lesson?: Lesson) {
  if (!lesson) {
    return !lessonStatus.inProgress
  }
  const lessonConfig = loadLessonConfig()

  return !lessonStatus.inProgress && (lessonIsNotInThePast(lesson) || lessonConfig.editPastLessons)
}

export function persistPlayerEvent(lesson: Lesson, event: PlayerEvent) {
  // TODO when processing for display, you need to consider the scenario where the lesson was completed, but it was clicked on again

  const now = new Date()

  const existingHistory = loadHistory(lesson)

  // TODO the events get duplicated for some reason
  // console.log(existingHistory);
  if (!existingHistory) {
    createHistory(lesson, event)

    return
  }

  let updatedHistory = {
    ...existingHistory,
  }

  if (['stop', 'forceStop'].includes(event.operation)) {
    updatedHistory.stoppedAt = now
  }

  const previousEvent = existingHistory.events[existingHistory.events.length - 1]

  if (previousEvent.event.operation === 'pause') {
    updatedHistory.pausedTime = calculateInterval(now, previousEvent.createdAt)
  }

  const newEvents = [
    {
      event,
      createdAt: now,
    },
  ]

  update(updatedHistory, newEvents)
}

export function lessonWasAlreadyPlayed(lesson: Lesson): boolean {
  if (shouldBeHistoryV2(lesson)) {
    const history = loadHistoryV2(lesson.id)

    return history.isSome()
  }
  const history = loadHistory(lesson)

  return history ? history.events.length > 0 : false
}

export const lessonHistory: Readable<ProcessedHistory | null> = derived(
  currentLesson,
  (lesson, set) => {
    if (!lesson) {
      set(null)
    } else {
      const history = loadHistory(lesson)

      if (!history) {
        set(null)

        return
      }

      if (!history.stoppedAt) {
        console.error('Lesson does not have stoppedAt')

        set(null)

        return
      }

      const totalTime = (history.stoppedAt.getTime() - history.startedAt.getTime()) / 1000
      const totalActiveTime = totalTime - history.pausedTime

      const lessonItemStatuses: Record<string, LessonItemStatus> = history.events.reduce((carry, event) => {
        if (event.event.payload) {
          const lessonItemId = event.event.payload.lessonItemId

          if (carry['4dc76536-45f6-444d-a9f5-1698241ea878']?.status === 'incomplete') {
            console.log('carry', carry)
          }

          if (lessonItemId) {
            carry = carry[lessonItemId]
              ? {
                  ...carry,
                  [lessonItemId]: setNewStatusForLessonItem(carry[lessonItemId], event.event.operation),
                }
              : {
                  ...carry,
                  [lessonItemId]: setStatusForLessonItem(lessonItemId, event.event.operation),
                }
          }
        }

        if (carry['4dc76536-45f6-444d-a9f5-1698241ea878']?.status === 'completedLessonItem') {
          console.log('carry', carry)
        }
        return carry
      }, {} as Record<string, LessonItemStatus>)

      const processedHistory: ProcessedHistory = {
        totalTime: Math.round(totalTime),
        totalActiveTime: Math.round(totalActiveTime),
        totalPauseTime: Math.round(history.pausedTime),
        lessonItemStatuses,
        lessonId: history.lessonId,
      }

      set(processedHistory ?? null)
    }
  },
  null as ProcessedHistory | null
)

export const lessonHasHistory: Readable<boolean> = derived(
  currentLesson,
  (lesson, set) => {
    set(lesson ? lessonWasAlreadyPlayed(lesson) : false)
  },
  false
)

function stopAllAudio() {
  stopAudio(countdown)
}
