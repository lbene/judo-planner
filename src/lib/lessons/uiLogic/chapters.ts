import { formatDuration } from '$lib/common/domain/common'
import type { Duration, Lesson } from '$lib/lessons/domain/lessons'
import { getLessonItem } from '$lib/lessons/domain/lesson'

export function getDefaultDuration(): Duration {
  return {
    minute: 5,
    second: 0,
    formatted: formatDuration(5, 0),
  }
}

export function getDuration(lessonItemId: string, chapterId: string, lesson?: Lesson) {
  if (!lesson) {
    return getDefaultDuration()
  }

  return getLessonItem(lesson, chapterId, lessonItemId).duration
}
