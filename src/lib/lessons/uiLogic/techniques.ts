import type { Technique } from '$lib/lessons/domain/techniques'
import { addNewTechnique, editTechnique } from '$lib/lessons/domain/techniques'
import { get, writable } from 'svelte/store'
import type { Circuit } from '../../../routes/[lang]/settings/technique/_domain/circuits'
import { loadTechniques } from '$lib/lessons/storage/techniquesLocalStorage'

export type SearchTechniqueEvent = {
  search: SearchTechniqueEventPayload
}

export type SearchTechniqueEventPayload = {
  techniques: Technique[]
}

export type SearchCircuitEvent = {
  search: SearchCircuitEventPayload
}

export type SearchCircuitEventPayload = {
  circuits: Circuit[]
}

export function addNew(name: string, media?: string) {
  const list = addNewTechnique(name, media)

  return saveToStore(list)
}

export function edit(id: string, name: string, media?: string) {
  const list = editTechnique(id, name, media)

  saveToStore(list)
}

export function loadTechniquesByIds(ids: string[], sortCallback?: (a: Technique, b: Technique) => number): Technique[] {
  const result = get(techniqueStore).filter((value) => ids.includes(value.id))

  return sortCallback ? result.sort(sortCallback) : result
}

export function getTechniqueNames(techniques: Technique[]): Record<string, string> {
  return techniques.reduce((carry, technique) => {
    carry[technique.id] = technique.name

    return carry
  }, {} as Record<string, string>)
}

export const techniqueStore = writable<Technique[]>(getSortedTechniques())

export function debounce(callback: Function, interval: number, immediate = false) {
  let timeout: NodeJS.Timeout | undefined

  return function () {
    const context: Function = this,
      args: any = arguments

    const later = function () {
      timeout = undefined
      if (!immediate) callback.apply(context, args)
    }

    var callNow = immediate && !timeout

    clearTimeout(timeout)
    timeout = setTimeout(later, interval)

    if (callNow) callback.apply(context, args)
  }
}

export function search<T extends Circuit | Technique>(searchText: string, originalElements: T[]): T[] {
  if (searchText.length > 1) {
    return originalElements.filter((value) => value.name.toLowerCase().includes(searchText))
  }

  return [...originalElements]
}

export function saveToStore(list: Technique[]) {
  const breakTech = list[0]

  const finalList = [breakTech, ...list.slice(1).sort(compareTechniques)]

  techniqueStore.set(finalList)

  return finalList
}

function getSortedTechniques() {
  const techniques = loadTechniques()

  if (techniques.length === 0) {
    return []
  }

  const breakTech = techniques[0]

  return [breakTech, ...techniques.slice(1).sort(compareTechniques)]
}

function compareTechniques(a: Technique, b: Technique) {
  return a.name < b.name ? -1 : a.name > b.name ? 1 : 0
}
