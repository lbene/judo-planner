import type {
  AddLessonEventPayload,
  BaseLessonEventPayload,
} from '$lib/lessons/components/lessonNavigation/presentation'
import { goto } from '$app/navigation'
import { appendToSearchParams, getDayNumber } from '$lib/common/domain/common'
import type { Lesson } from '$lib/lessons/domain/lessons'
import { deleteLesson, loadLessonsOfTheWeek, saveNewLesson, updateLesson } from '$lib/lessons/domain/lessons'

export function getSelectedDayFromUrl(url: URL) {
  return url.searchParams.get('day') ? Number(url.searchParams.get('day')) : new Date().getDate()
}

export function getClosestDayWithLesson(url: URL, lessons: Record<number, Lesson> | null) {
  const today = new Date().getDate()

  let selectedDay = url.searchParams.get('day') ? Number(url.searchParams.get('day')) : today

  if (lessons) {
    if (!lessons[selectedDay]) {
      selectedDay = Object.entries(lessons)[0][0] ? Number(Object.entries(lessons)[0][0]) : today
    }
  }

  return selectedDay
}

export function loadLessons(year: string, month: string, week: string) {
  return loadLessonsOfTheWeek(year, month, week)
}

export function addLesson(year: string, month: string, week: string, day: string): Record<number, Lesson> | null {
  saveNewLesson(new Date(Number(year), Number(month), Number(day)))

  return loadLessonsOfTheWeek(year, (Number(month) + 1).toString(), week)
}

export function removeLesson(
  event: CustomEvent<BaseLessonEventPayload>,
  lessons: Record<number, Lesson>
): Record<number, Lesson> | null {
  const lesson = lessons[Number(event.detail.day)]

  return deleteLesson(lesson)
}

export function updateLessonTitle(
  event: CustomEvent,
  lessons: Record<number, Lesson> | null,
  selectedDay: number
): string {
  const title = event.detail.title

  if (lessons) {
    const updatedLesson = { ...lessons[selectedDay] }

    updatedLesson.title = title

    updateLesson(updatedLesson)
  }

  return title
}

export function updateLessonDate(
  event: CustomEvent,
  lessons: Record<number, Lesson> | null,
  selectedDay: number,
  year: string,
  month: string,
  week: string
): Record<number, Lesson> | null {
  if (lessons) {
    const originalLesson = { ...lessons[selectedDay] }
    const updatedLesson = { ...originalLesson }

    updatedLesson.date = event.detail.date

    updateLesson(updatedLesson, originalLesson)

    // TODO decide how to load the lessons, based on the original lesson or the new one.
    // This way the bug in navigateToUpdatedLesson() might also be fixed, or at least determine the whole URL from there.
    return loadLessonsOfTheWeek(year, month, week)
  }

  return lessons
}

export function navigateToAddedLesson(
  url: URL,
  event: CustomEvent<AddLessonEventPayload>,
  year: string,
  month: string,
  week: string
) {
  goto(
    appendToSearchParams(url, [
      {
        name: 'year',
        value: year,
      },
      {
        name: 'month',
        value: month,
      },
      {
        name: 'week',
        value: week,
      },
      {
        name: 'day',
        value: event.detail.day,
      },
    ])
  )
}

export function navigateToSelectedLesson(url: URL, event: CustomEvent<BaseLessonEventPayload>) {
  goto(
    appendToSearchParams(url, [
      {
        name: 'day',
        value: event.detail.day,
      },
    ])
  )
}

export function navigateToNextLesson(url: URL, lessons: Record<number, Lesson> | null) {
  const dayOfNextLesson = lessons ? Object.entries(lessons)[0][0] : null

  if (dayOfNextLesson) {
    goto(
      appendToSearchParams(url, [
        {
          name: 'day',
          value: getDayNumber(Number(dayOfNextLesson)),
        },
      ])
    )
  }
}

export function navigateToUpdatedLesson(url: URL, event: CustomEvent) {
  // TODO there is a potential bug here, when the week or month is modified we will have to jump to the proper values for those as well
  goto(
    appendToSearchParams(url, [
      {
        name: 'day',
        value: getDayNumber(event.detail.date.getDate()),
      },
    ])
  )
}
