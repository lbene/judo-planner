import type { Lesson } from '$lib/lessons/domain/lessons'
import { getCurrentWeek } from '$lib/common/domain/common'
import { type Optional, optionalOf } from '$lib/common/domain/optional'

export function loadLesson(lessonId: string): Promise<Lesson> {
  return new Promise<Lesson>((resolve, reject) => {
    const date = loadDateFromIndex(lessonId)

    const lessonsPerWeek = localStorage.getItem(getLocalStorageId(date))

    if (!lessonsPerWeek) {
      reject(new Error('No lessons this week: ' + new Date().toString()))
      return
    }

    try {
      const lessons = deserialize(lessonsPerWeek)

      const lesson = lessons.find((lesson) => lesson.id === lessonId)

      if (lesson) {
        resolve(lesson)
      } else {
        reject('Could not find lesson with id: ' + lessonId)
      }
    } catch (e) {
      console.error('Could not deserialize')
      reject(e)
    }
  })
}

function getLocalStorageId(date: Date): string {
  return `year:${date.getFullYear()}-month:${date.getMonth() + 1}-week:${getCurrentWeek(date)}`
}

function deserialize(value: string): Lesson[] {
  return JSON.parse(value, (key, value) => {
    if (key === 'createdAt' || key === 'date') {
      return new Date(Date.parse(value))
    }
    return value
  })
}

function loadDateFromIndex(lessonId: string): Date {
  return optionalOf(localStorage.getItem('index_lesson_dates'))
    .flatMap((value) => getDateFromData(JSON.parse(value), lessonId))
    .getOrThrow(`Lesson with id ${lessonId} not found in index`)
}

function getDateFromData(data: Record<string, string>, lessonId: string): Optional<Date> {
  return optionalOf(Object.entries(data).find((value) => value[1] === lessonId)).map((value) => new Date(value[0]))
}
