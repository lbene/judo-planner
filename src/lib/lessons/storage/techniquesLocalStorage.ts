import type { Technique } from '$lib/lessons/domain/techniques'
import { techniquesStorageId } from '$lib/lessons/domain/techniques'

export function loadTechniques(): Technique[] {
  const rawTechniques = localStorage.getItem(techniquesStorageId)

  if (rawTechniques === null) {
    return []
  }

  return transformToTechniques(rawTechniques)
}

export function transformToTechniques(localStorageContent: string): Technique[] {
  if (localStorageContent === '') {
    return []
  }

  return JSON.parse(localStorageContent)
}
