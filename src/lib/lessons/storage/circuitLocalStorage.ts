import type { Circuit } from '../../../routes/[lang]/settings/technique/_domain/circuits'

const storageId = 'circuits'
const storageIdForTemp = 'tempCircuit'

export function saveNewCircuit(circuit: Circuit): Circuit[] {
  const rawResults = localStorage.getItem(storageId)

  if (!rawResults) {
    persist([circuit])

    return [circuit]
  }

  const circuits = JSON.parse(rawResults) as Circuit[]

  circuits.push(circuit)

  persist(circuits)

  return circuits
}

export function resetTempCircuit() {
  localStorage.removeItem(storageIdForTemp)
}

export function saveTempCircuit(circuit: Circuit): Circuit {
  persistTemp(circuit)

  return circuit
}

export function loadTempCircuit(): Circuit | null {
  const circuit = localStorage.getItem(storageIdForTemp)

  if (!circuit) {
    return null
  }

  return JSON.parse(circuit) as Circuit
}

export function saveExistingCircuit(circuit: Circuit): Circuit[] {
  const rawResults = localStorage.getItem(storageId)

  if (!rawResults) {
    persist([circuit])

    return [circuit]
  }

  const circuits = JSON.parse(rawResults) as Circuit[]

  const newCircuits = circuits.map((value) => {
    if (value.id === circuit.id) {
      return circuit
    }

    return value
  })

  persist(newCircuits)

  return newCircuits
}

export function loadAllCircuits(): Promise<Circuit[]> {
  return new Promise((resolve, reject) => {
    try {
      resolve(loadAll())
    } catch (e) {
      reject(e)
    }
  })
}

export function loadAll(): Circuit[] {
  const rawResults = localStorage.getItem(storageId)

  if (!rawResults) {
    return []
  }

  return JSON.parse(rawResults) as Circuit[]
}

export function deleteById(id: string): Circuit[] {
  const rawResults = localStorage.getItem(storageId)

  if (!rawResults) {
    return []
  }

  const circuits = (JSON.parse(rawResults) as Circuit[]).filter((circuit) => circuit.id !== id)

  persist(circuits)

  return circuits
}

function persist(circuits: Circuit[]) {
  localStorage.setItem(storageId, JSON.stringify(circuits))
}

function persistTemp(circuit: Circuit) {
  localStorage.setItem(storageIdForTemp, JSON.stringify(circuit))
}
