import type { LessonHistory, PlayableLessonItemId } from '$lib/lessons/domain/player'
import { formatDateIso, formatDateStandard } from '$lib/common/domain/common'
import type { Optional } from '$lib/common/domain/optional'
import { optionalOf } from '$lib/common/domain/optional'
import type { Note } from '$lib/lessons/domain/history'

export function saveHistory(history: LessonHistory) {
  const id = getLocalStorageId(history.lessonId)

  localStorage.setItem(id, serialize(history))
}

export function loadHistory(lessonId: string): Optional<LessonHistory> {
  const id = getLocalStorageId(lessonId)

  const historyRaw: Optional<string> = optionalOf(localStorage.getItem(id))

  return historyRaw.map<LessonHistory>((value) => deserialize(value))
}

export function deleteHistory(lessonId: string) {
  const id = getLocalStorageId(lessonId)

  localStorage.removeItem(id)
}

export function loadNotes(history: Optional<LessonHistory>): Note[] {
  return history.map((history) => history.notes).getOrElse([])
}

function getLocalStorageId(lessonId: PlayableLessonItemId) {
  return `v2_lesson_history_${lessonId}`
}

function serialize(history: LessonHistory) {
  return JSON.stringify(history, function (key, value) {
    if (this[key] instanceof Date) {
      const date = this[key]

      try {
        return formatDateIso(date)
      } catch (error) {
        console.log(error)
        console.error('Could not format date with key: ' + key)
        console.error('Could not format date', date)
        console.error('Standard format would be' + formatDateStandard(date))
      }
    }

    return value
  })
}

function deserialize(value: string): LessonHistory {
  return JSON.parse(value, (key, value) => {
    const dateKeys = ['createdAt', 'startedAt', 'stoppedAt', 'updatedAt']

    if (dateKeys.includes(key)) {
      return new Date(Date.parse(value))
    }

    return value
  })
}
