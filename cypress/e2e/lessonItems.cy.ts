import { visitHomePage } from '../support/actions/navigation'
import { setupTechniques } from '../support/actions/techniques'
import { formatDateStandard, getSameDayNextWeek, getTomorrow, getYesterday } from '../support/actions/dates'
import { setupLocalStorage } from '../support/actions/setup'
import { searchInTechniqueModal } from '../support/actions/lessons'

describe('This is a file where we test lesson items manipulation', () => {
  beforeEach(() => {
    visitHomePage('ro')
    setupTechniques()
  })

  it('search lesson items', () => {
    const today = formatDateStandard(new Date())

    cy.get(`[data-test="emptyDay:${today}"]`).click()

    cy.get('[data-test="addLessonItems"').then((items) => {
      items[0].click()
    })

    searchInTechniqueModal('image')
    // TODO research if data-test="techniqueId" can be used and how it would impact this test
    cy.get(`[data-test="technique"]`).should((items) => {
      expect(items.length).eq(1)
      expect(items[0]).to.contain('Image')
    })

    searchInTechniqueModal('video')
    cy.get(`[data-test="technique"]`).should((items) => {
      expect(items.length).eq(1)
      expect(items[0]).to.contain('Video')
    })

    searchInTechniqueModal('empty')
    cy.get(`[data-test="technique"]`).should((items) => {
      expect(items.length).eq(1)
      expect(items[0]).to.contain('Empty')
    })
  })

  it('temporary lesson items match with what has been added', () => {
    // at the moment the name of the elements after being added is hard to obtain
    const today = formatDateStandard(new Date())

    cy.get(`[data-test="emptyDay:${today}"]`).click()
    cy.get('[data-test="addLessonItems"').then((items) => {
      items[0].click()
    })

    cy.get('[data-test="technique"]').then((items) => {
      cy.wrap(items[1]).within(() => {
        cy.get('[name="minute"]').click().clear().type('01')
        cy.get('[name="second"]').click().clear().type('30')
        cy.get('[data-test="add-technique"]').click()
      })
    })

    cy.get('[data-test="technique"]').then((items) => {
      cy.wrap(items[2]).within(() => {
        cy.get('[name="minute"]').click().clear().type('02')
        cy.get('[name="second"]').click().clear().type('15')
        cy.get('[data-test="add-technique"]').click()
      })

      cy.get('[data-test="chosen-item"]').then((items) => {
        expect(items[0]).to.contain.text('01:30')
        expect(items[1]).to.contain.text('02:15')
      })
    })
  })

  it('remove temporary lesson items', () => {
    // add more lesson items and remove some of them
    const today = formatDateStandard(new Date())

    cy.get(`[data-test="emptyDay:${today}"]`).click()
    cy.get('[data-test="addLessonItems"').then((items) => {
      items[0].click()
    })

    cy.get('[data-test="technique"]').then((items) => {
      cy.wrap(items[1]).within(() => {
        cy.get('[name="minute"]').click().clear().type('01')
        cy.get('[name="second"]').click().clear().type('30')
        cy.get('[data-test="add-technique"]').click()
      })
    })

    cy.get('[data-test="technique"]').then((items) => {
      cy.wrap(items[2]).within(() => {
        cy.get('[name="minute"]').click().clear().type('02')
        cy.get('[name="second"]').click().clear().type('15')
        cy.get('[data-test="add-technique"]').click()
      })
    })

    cy.get('[data-test="remove-item"]').then((items) => {
      items[0].click()
    })
  })

  it('remove temporary lesson items2', () => {
    // add 3 lesson items and remove the 2nd one. Check the remaining ones
    const today = formatDateStandard(new Date())

    cy.get(`[data-test="emptyDay:${today}"]`).click()
    cy.get('[data-test="addLessonItems"').then((items) => {
      items[0].click()
    })

    cy.get('[data-test="technique"]').then((items) => {
      cy.wrap(items[1]).within(() => {
        cy.get('[name="minute"]').click().clear().type('01')
        cy.get('[name="second"]').click().clear().type('30')
        cy.get('[data-test="add-technique"]').click()
      })
    })

    cy.get('[data-test="technique"]').then((items) => {
      cy.wrap(items[2]).within(() => {
        cy.get('[name="minute"]').click().clear().type('02')
        cy.get('[name="second"]').click().clear().type('15')
        cy.get('[data-test="add-technique"]').click()
      })
    })

    cy.get('[data-test="technique"]').then((items) => {
      cy.wrap(items[3]).within(() => {
        cy.get('[name="minute"]').click().clear().type('03')
        cy.get('[name="second"]').click().clear().type('45')
        cy.get('[data-test="add-technique"]').click()
      })
    })

    cy.get('[data-test="remove-item"]').then((items) => {
      items[1].click()
    })

    cy.get('[data-test="chosen-item"]').then((items) => {
      expect(items.length).eq(2)
      expect(items[0]).to.contain.text('01:30')
      expect(items[1]).to.contain.text('03:45')
    })
  })

  it('remove all temporary lesson items', () => {
    // add more lesson items and remove all of them
    const today = formatDateStandard(new Date())

    cy.get(`[data-test="emptyDay:${today}"]`).click()
    cy.get('[data-test="addLessonItems"').then((items) => {
      items[0].click()
    })

    cy.get('[data-test="technique"]').then((items) => {
      cy.wrap(items[1]).within(() => {
        cy.get('[name="minute"]').click().clear().type('01')
        cy.get('[name="second"]').click().clear().type('30')
        cy.get('[data-test="add-technique"]').click()
      })
    })

    cy.get('[data-test="technique"]').then((items) => {
      cy.wrap(items[2]).within(() => {
        cy.get('[name="minute"]').click().clear().type('02')
        cy.get('[name="second"]').click().clear().type('15')
        cy.get('[data-test="add-technique"]').click()
      })
    })

    cy.get('[data-test="remove-item"]').then((items) => {
      items[1].click()
      items[0].click()
    })

    cy.get('[data-test="chosen-item"]').should('not.exist')
  })
  it('save temporary lesson items', () => {
    // add more lesson items and save
    //   chapter duration is updated and lesson duration in the plan section is also updated
    //   added lesson items are displayed in the same order they were added

    const today = formatDateStandard(new Date())

    cy.get(`[data-test="emptyDay:${today}"]`).click()
    cy.get('[data-test="addLessonItems"').then((items) => {
      items[0].click()
    })

    cy.get('[data-test="technique"]').then((items) => {
      cy.wrap(items[1]).within(() => {
        cy.get('[name="minute"]').click().clear().type('01')
        cy.get('[name="second"]').click().clear().type('30')
        cy.get('[data-test="add-technique"]').click()
      })
    })

    cy.get('[data-test="technique"]').then((items) => {
      cy.wrap(items[2]).within(() => {
        cy.get('[name="minute"]').click().clear().type('02')
        cy.get('[name="second"]').click().clear().type('15')
        cy.get('[data-test="add-technique"]').click()
      })
    })

    cy.get('[data-test="submit"]').click()
    // TODO change to data-test when it's implemented
    cy.get('[class="duration-container svelte-rsyl9s"]').contains('03:45')
    cy.get('[data-test="lineItem:totalTime"]').contains('03:45')

    cy.get('[class="element-duration"]').then((items) => {
      expect(items[0]).to.contain.text('01:30')
      expect(items[1]).to.contain.text('02:15')
    })
  })

  it('clone lesson item', () => {
    // clone / duplicate a lesson item
    //   the item is added at the end of the chapter
    const today = formatDateStandard(new Date())

    cy.get(`[data-test="emptyDay:${today}"]`).click()
    cy.get('[data-test="addLessonItems"').then((items) => {
      items[0].click()
    })

    cy.get('[data-test="technique"]').then((items) => {
      cy.wrap(items[1]).within(() => {
        cy.get('[name="minute"]').click().clear().type('01')
        cy.get('[name="second"]').click().clear().type('30')
        cy.get('[data-test="add-technique"]').click()
      })
    })

    cy.get('[data-test="technique"]').then((items) => {
      cy.wrap(items[2]).within(() => {
        cy.get('[name="minute"]').click().clear().type('01')
        cy.get('[name="second"]').click().clear().type('15')
        cy.get('[data-test="add-technique"]').click()
      })
    })
    cy.get('[data-test="submit"]').click()

    cy.get('[data-test="lessonItem"]').then((items) => {
      cy.wrap(items[0]).within(() => {
        cy.get('[data-test="clone"]').click({ force: true })
      })
    })

    cy.get('[class="element-duration"]').then((items) => {
      expect(items.length).eq(4)
      expect(items[0]).to.contain.text('01:30')
      expect(items[2]).to.contain.text('01:30')
    })
    cy.get('.element-title').then((items) => {
      expect(items[0]).to.contain.text('Empty technique')
      expect(items[2]).to.contain.text('Empty technique')
    })

    cy.get('.element-title').then((items) => {
      const originalTitle = items[0].innerText
      const clonedTitle = items[2].innerText
      expect(originalTitle).to.equal(clonedTitle)
    })
  })

  it('edit lesson item duration', () => {
    // edit the duration of the lesson item
    //   lesson breakdown and chapter duration are updated
    const today = formatDateStandard(new Date())

    cy.get(`[data-test="emptyDay:${today}"]`).click()
    cy.get('[data-test="addLessonItems"').then((items) => {
      items[0].click()
    })

    cy.get('[data-test="technique"]').then((items) => {
      cy.wrap(items[1]).within(() => {
        cy.get('[name="minute"]').click().clear().type('01')
        cy.get('[name="second"]').click().clear().type('30')
        cy.get('[data-test="add-technique"]').click()
      })
    })

    cy.get('[data-test="technique"]').then((items) => {
      cy.wrap(items[2]).within(() => {
        cy.get('[name="minute"]').click().clear().type('01')
        cy.get('[name="second"]').click().clear().type('15')
        cy.get('[data-test="add-technique"]').click()
      })
    })
    cy.get('[data-test="submit"]').click()

    cy.get('[data-test="lessonItem"]').then((items) => {
      cy.wrap(items[0]).within(() => {
        cy.get('[data-test="edit"]').click({ force: true })
      })
    })
    cy.get('[data-test="formModal"]').within(() => {
      cy.get('.time').then((items) => {
        cy.wrap(items[0]).click().clear().type('02')
        cy.wrap(items[1]).click().clear().type('05')
      })
      cy.get('[data-test="submit"]').click()
    })

    cy.get('[class="duration-container svelte-rsyl9s"]').contains('03:20')
    cy.get('[data-test="lineItem:totalTime"]').contains('03:20')

    cy.get('[class="element-duration"]').then((items) => {
      expect(items[0]).to.contain.text('02:05')
      expect(items[1]).to.contain.text('01:15')
    })
  })

  // it('delete lesson item', () => {
  //   // delete lesson item
  //   //   lesson breakdown and chapter duration are updated
  // })
})
