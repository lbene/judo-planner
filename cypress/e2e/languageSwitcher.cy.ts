import { visitTechniquesPage } from '../support/actions/navigation'

describe('Language switcher', () => {
  it('Romanian is the default language', () => {
    visitTechniquesPage('ro')

    const element = cy.get('[data-test="languageSwitcher"]')

    element.contains('RO')
    cy.get('custom-select').shadow().find('ul').should('not.be.visible')
  })

  it('Language switcher dropdown is displayed', () => {
    visitTechniquesPage('ro')

    cy.get('[data-test="languageSwitcher"]').click()

    cy.get('custom-select').shadow().find('ul').should('be.visible')
  })

  it('Romanian is the first language in the dropdown', () => {
    visitTechniquesPage('ro')

    const listItems = cy.get('custom-select').shadow().find('li')

    // We want to check that the position of Romanian is 0
    const position = 0

    // The "eq" function returns the DOM element at a specific index: https://docs.cypress.io/api/commands/eq
    // In our case there are 3 list items, so we can use eq(0), eq(1) or eq(2)
    listItems.eq(position).should('contain.text', 'RO')
  })

  it('Hungarian is the 2nd language in the dropdown', () => {
    visitTechniquesPage('ro')

    // We want to check that the position of Hungarian is 1
    const position = 1

    cy.get('custom-select').shadow().find('li').eq(position).should('contain.text', 'HU')
  })

  it('English is the 3rd language in the dropdown', () => {
    visitTechniquesPage('ro')

    // We want to check that the position of English is 2
    const position = 2

    cy.get('custom-select').shadow().find('li').eq(position).should('contain.text', 'EN')
  })
})

export {}
