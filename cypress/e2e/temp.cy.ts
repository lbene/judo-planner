import { visitHomePage, visitTechniquesPage } from '../support/actions/navigation'
import { setupTechniques } from '../support/actions/techniques'
import { formatDateStandard } from '../support/actions/dates'

describe('This is a file where we can test out individual tests', () => {
  beforeEach(() => {
    visitHomePage('ro')
    setupTechniques()
  })

  it('edit lesson item duration', () => {
    // edit the duration of the lesson item
    //   lesson breakdown and chapter duration are updated
    const today = formatDateStandard(new Date())

    cy.get(`[data-test="emptyDay:${today}"]`).click()
    cy.get('[data-test="addLessonItems"').then((items) => {
      items[0].click()
    })

    cy.get('[data-test="technique"]').then((items) => {
      cy.wrap(items[1]).within(() => {
        cy.get('[name="minute"]').click().clear().type('01')
        cy.get('[name="second"]').click().clear().type('30')
        cy.get('[data-test="add-technique"]').click()
      })
    })

    cy.get('[data-test="technique"]').then((items) => {
      cy.wrap(items[2]).within(() => {
        cy.get('[name="minute"]').click().clear().type('01')
        cy.get('[name="second"]').click().clear().type('15')
        cy.get('[data-test="add-technique"]').click()
      })
    })
    cy.get('[data-test="submit"]').click()

    cy.get('[data-test="lessonItem"]').then((items) => {
      cy.wrap(items[0]).within(() => {
        cy.get('[data-test="edit"]').click({ force: true })
      })
    })
    cy.get('[data-test="formModal"]').within(() => {
      cy.get('.time').then((items) => {
        cy.wrap(items[0]).click().clear().type('02')
        cy.wrap(items[1]).click().clear().type('05')
      })
      cy.get('[data-test="submit"]').click()
    })

    cy.get('[class="duration-container svelte-rsyl9s"]').contains('03:20')
    cy.get('[data-test="lineItem:totalTime"]').contains('03:20')

    cy.get('[class="element-duration"]').then((items) => {
      expect(items[0]).to.contain.text('02:05')
      expect(items[1]).to.contain.text('01:15')
    })
  })
})
