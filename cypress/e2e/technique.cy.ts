import { visitTechniquesPage } from '../support/actions/navigation'
import { addWazaElement } from '../support/actions/techniques'
import { generateArrayOfStrings, generateRandomString } from '../support/actions/strings'

describe('Technique page', () => {
  it('Displays the add button', () => {
    visitTechniquesPage('en')

    cy.get('[data-test="addTech"]').should('exist')
  })

  it('Add technique modal is displayed', () => {
    visitTechniquesPage('en')

    cy.get('[data-test="addTech"]').click()

    cy.get('.modal').should('exist').contains('Name')
  })

  it('Back CTA is displayed', () => {
    visitTechniquesPage('en')

    cy.get('.lessons').should('exist').contains('Lessons')
  })

  it('Add and remove waza element', () => {
    visitTechniquesPage('en')
    addWazaElement('Test11')

    // TODO later when we learn how to set up local storage with data, extract this
    // into an individual test
    cy.get('[data-test="techniqueList"]').contains('Test11')

    cy.get('[data-test="delete"]').click()
    cy.contains('Test11').should('not.exist')
  })

  it('Backup option displayed and hidden', () => {
    visitTechniquesPage('en')

    // to move this to backup.cy.ts and to change the locator
    cy.get('[data-test="settings"] > .control-container').click()

    cy.get('.element').contains('Backup')
    cy.get('[data-test="backup"]').then((items) => {
      console.log(items)
    })
    cy.get('[data-test="backup"]').click()
    cy.get('.element').should('not.exist')
  })

  it('Partial search waza right after adding it', () => {
    visitTechniquesPage('en')
    addWazaElement('Test1')
    addWazaElement('Magda')

    cy.get('[name="search"]').click().type('Mag')
    cy.get('[data-test="techniqueList"]').contains('Magda')
  })

  it('Removed element should not appear in search results', () => {
    visitTechniquesPage('en')
    addWazaElement('Test1')

    cy.get('[data-test="techniqueList"]').contains('Test1')

    cy.get('[data-test="delete"]').click()

    cy.get('[name="search"]').click().type('Test1')
    cy.contains('Test1').should('not.exist')
  })

  it('Goes to lessons page and returns to technique', () => {
    visitTechniquesPage('en')

    cy.get('.lessons').click()
    cy.get('.menu-item').click()
    cy.get('.lessons').should('exist')
  })

  it('Adds waza element with video url', () => {
    visitTechniquesPage('en')
    addWazaElement('Test1', 'https://www.youtube.com/watch?v=RJtTVQ3ONKw')
    // cy.get('[data-test="video"]').trigger('mouseover')
    // cy.get('[data-test="tooltip"]').contains('View video')
    // TODO analyze hover in cypress
    cy.get('[data-test="video"]').click()
    cy.get('iframe').should('have.attr', 'src', 'https://www.youtube.com/embed/RJtTVQ3ONKw')
  })

  it('Adds waza element with invalid video type url', () => {
    visitTechniquesPage('en')
    addWazaElement('Test1', 'https://vimeo.com/157728333')
    // cy.get('[data-test="video"]').trigger('mouseover')
    // cy.get('[data-test="tooltip"]').contains('View video')
    // TODO analyze hover in cypress
    cy.get('[data-test="video"]').should('not.exist')
  })

  it('Adds waza element with jpeg image url', () => {
    visitTechniquesPage('en')
    addWazaElement('Test2', 'https://cmac-judo.com/_Media/uki-waza_med_hr.jpeg')
    // cy.get('[data-test="video"]').trigger('mouseover')
    // cy.get('[data-test="tooltip"]').contains('View video')
    // TODO analyze hover in cypress
    cy.get('[data-test="image"]').click()
    cy.get('img').should('have.attr', 'src', 'https://cmac-judo.com/_Media/uki-waza_med_hr.jpeg')
  })

  it('Adds waza element with jpg image url', () => {
    visitTechniquesPage('en')
    addWazaElement('Test3', 'https://upload.wikimedia.org/wikipedia/commons/3/36/Uki-waza.jpg')
    // cy.get('[data-test="video"]').trigger('mouseover')
    // cy.get('[data-test="tooltip"]').contains('View video')
    // TODO analyze hover in cypress
    cy.get('[data-test="image"]').click()
    cy.get('img').should('have.attr', 'src', 'https://upload.wikimedia.org/wikipedia/commons/3/36/Uki-waza.jpg')
  })

  it('Adds waza element with png image url', () => {
    visitTechniquesPage('en')
    addWazaElement('Test4', 'https://upload.wikimedia.org/wikipedia/fi/a/a1/WAZA_logo.png')
    // cy.get('[data-test="video"]').trigger('mouseover')
    // cy.get('[data-test="tooltip"]').contains('View video')
    // TODO analyze hover in cypress
    cy.get('[data-test="image"]').click()
    cy.get('img').should('have.attr', 'src', 'https://upload.wikimedia.org/wikipedia/fi/a/a1/WAZA_logo.png')
  })

  it('Adds waza element with gif image url', () => {
    visitTechniquesPage('en')
    addWazaElement('Test5', 'https://i.imgur.com/8QNKJlN.gif')
    // cy.get('[data-test="video"]').trigger('mouseover')
    // cy.get('[data-test="tooltip"]').contains('View video')
    // TODO analyze hover in cypress
    cy.get('[data-test="image"]').click()
    cy.get('img').should('have.attr', 'src', 'https://i.imgur.com/8QNKJlN.gif')
  })

  it('Adds waza element with invalid image type url', () => {
    visitTechniquesPage('en')
    addWazaElement('Test6', 'https://www.ikangai.com/wp-content/uploads/2021/12/img-Shokunin-waza.svg')
    // cy.get('[data-test="video"]').trigger('mouseover')
    // cy.get('[data-test="tooltip"]').contains('View video')
    // TODO analyze hover in cypress
    cy.get('[data-test="image"]').should('not.exist')
  })

  it('Adds waza elements, break appears first in elements list', () => {
    visitTechniquesPage('en')
    addWazaElement('Test7')
    addWazaElement('Test8')
    addWazaElement('Test9')
    cy.get('[data-test="techniqueList"]').then((items) => {
      expect(items[0]).to.contain.text('Break')
    })
  })

  it('Adds waza elements, delete one, break appears first in elements list', () => {
    visitTechniquesPage('en')
    const firstTechnique = addWazaElement('Test11')
    addWazaElement('Test10')

    firstTechnique.then((id: string) => {
      cy.get(`[data-test="${id}"]`).within((item) => {
        cy.get('[data-test="delete"]').click()
      })
    })

    cy.get('[data-test="techniqueList"]').then((items) => {
      expect(items[0]).to.contain.text('Break')
    })
  })

  it('Add waza modal can be closed clicking X', () => {
    visitTechniquesPage('en')
    cy.get('[data-test="addTech"]').click()
    cy.get('[data-test="formModal"]').should('exist')
    cy.get('.close > .fa-solid').click()
    cy.get('[data-test="formModal"]').should('not.exist')
  })
  // TODO test modal can be closed clicking on the outside

  it('View waza modal can be closed clicking X', () => {
    visitTechniquesPage('en')
    const firstTechnique = addWazaElement('Test11')
    addWazaElement('Test10')

    firstTechnique.then((id: string) => {
      cy.get(`[data-test="${id}"]`).within((item) => {
        cy.get('[data-test="edit"]').click()
      })
    })

    cy.get('[data-test="formModal"]').should('exist')
    cy.get('.close > .fa-solid').click()
    cy.get('[data-test="formModal"]').should('not.exist')
  })

  it('Adds waza elements, they are ordered alphabetically', () => {
    visitTechniquesPage('en')
    addWazaElement('W-waza')
    addWazaElement('B-waza')
    addWazaElement('A-waza')

    cy.get('.tech').then((items) => {
      expect(items[0]).to.contain.text('Break')
      expect(items[1]).to.contain.text('A-waza')
      expect(items[2]).to.contain.text('B-waza')
      expect(items[3]).to.contain.text('W-waza')
    })
  })

  it('Adds waza elements, they are ordered alphabetically1', () => {
    visitTechniquesPage('en')

    const numberOfNewWaza = 3
    const newWazaNames = generateArrayOfStrings(numberOfNewWaza)

    for (const name of newWazaNames) {
      addWazaElement(name)
    }

    const allWazaNames = ['Break', ...newWazaNames.sort()]

    cy.get('.tech')
      .then((items) => {
        const namesFromTheDom = []

        for (const item of items) {
          if (item.textContent) {
            const name = item.textContent.trim()
            namesFromTheDom.push(name)
          }
        }

        return namesFromTheDom
      })
      .should('deep.eq', allWazaNames)
  })

  it('Adds waza elements, they are ordered alphabetically2', () => {
    visitTechniquesPage('en')

    const numberOfNewWaza = 3
    const newWazaNames = generateArrayOfStrings(numberOfNewWaza)

    for (const name of newWazaNames) {
      addWazaElement(name)
    }

    const allWazaNames = ['Break', ...newWazaNames.sort()]

    cy.get('.tech')
      .then((items) => {
        return items.toArray().map((item) => item.textContent!.trim())
      })
      .should('deep.eq', allWazaNames)
  })

  // add tests for waza w video and w audio, check the tooltips and the modals and url's - done-ish - tooltips not yet
  // test keyboard accessibility: enter submits, escape closes modal, tab jumps to new element
})

export {}
