import { visitHomePage } from '../support/actions/navigation'
import { setupTechniques } from '../support/actions/techniques'
import { formatDateStandard, getSameDayNextWeek, getTomorrow, getYesterday } from '../support/actions/dates'
import { setupLocalStorage } from '../support/actions/setup'

describe('This is a file where we can test out individual tests', () => {
  beforeEach(() => {
    visitHomePage('ro')
    setupTechniques()
  })

  it('add empty lesson', () => {
    const formattedDate = formatDateStandard(new Date())

    cy.get(`[data-test="emptyDay:${formattedDate}"]`).click()
    cy.get('[data-test="title:plan"]').contains('Plan')
    cy.get('[data-test="lineItem:totalTime"] > span').contains('Timp total')

    cy.get('.structure').then((items) => {
      expect(items[0]).to.contain.text('Partea organizatorică')
      expect(items[1]).to.contain.text('Partea pregătitoare')
      expect(items[2]).to.contain.text('Partea fundamentală')
      expect(items[3]).to.contain.text('Partea de încheiere')
    })
  })

  it('add empty lesson 2 - the simple way', () => {
    const formattedDate = formatDateStandard(new Date())

    cy.get(`[data-test="emptyDay:${formattedDate}"]`).click()
    cy.get(`[data-test="edit:${formattedDate}"]`).should('exist')
  })

  it('edit lesson title', () => {
    const formattedDate = formatDateStandard(new Date())

    cy.get(`[data-test="emptyDay:${formattedDate}"]`).click()
    cy.get(`[data-test="edit:${formattedDate}"]`).click()
    cy.get('[data-test="formModal"] > .content').click()
    cy.get('input[name="title"]').type(' - 1')
    cy.get('[data-test="submit"]').click()
    cy.get('span [slot = "text2"]').contains('Titlu - 1')
  })

  it('edit lesson title by clearing initial text', () => {
    const formattedDate = formatDateStandard(new Date())

    cy.get(`[data-test="emptyDay:${formattedDate}"]`).click()
    cy.get(`[data-test="edit:${formattedDate}"]`).click()
    cy.get('[data-test="formModal"] > .content').click()
    cy.get('input[name="title"]').clear().type('test')
    cy.get('[data-test="submit"]').click()
    cy.get('span [slot = "text2"]').contains('test')
  })

  it('delete lesson', () => {
    const formattedDate = formatDateStandard(new Date())

    cy.get(`[data-test="emptyDay:${formattedDate}"]`).click()

    const deleteButton = cy.get(`[data-test="delete:${formattedDate}"]`)
    deleteButton.click()
    deleteButton.should('not.exist')
    // OR
    cy.get(`[data-test="emptyDay:${formattedDate}"]`).should('exist')
  })

  it('edit lesson date', () => {
    const today = formatDateStandard(new Date())
    const tomorrow = formatDateStandard(getTomorrow())

    cy.get(`[data-test="emptyDay:${today}"]`).click()
    cy.get(`[data-test="calendar:${today}"]`).click()
    cy.get('[data-test="formModal"]').click()
    cy.get('input[type="date"]').clear().type(`${tomorrow}`)
    cy.get('[data-test="submit"]').click()
    cy.get(`[data-test="emptyDay:${today}"]`).should('exist')
    cy.get(`[data-test="edit:${tomorrow}"]`).should('exist')
  })
  it('verify clone modal title', () => {
    const today = formatDateStandard(new Date())

    cy.get(`[data-test="emptyDay:${today}"]`).click()
    cy.get(`[data-test="clone:${today}"]`).click()
    cy.get('.header').contains('Clonare lecţie')
  })

  it('clones successfully in the current week', () => {
    // use getTomorrow to get the date
    const today = formatDateStandard(new Date())
    const tomorrow = formatDateStandard(getTomorrow())

    cy.get(`[data-test="emptyDay:${today}"]`).click()
    cy.get(`[data-test="clone:${today}"]`).click()

    cy.get('input[type="date"]').clear().type(`${tomorrow}`)
    cy.get('[data-test="submit"]').click()

    cy.get(`[data-test="edit:${today}"]`).should('exist')
    cy.get(`[data-test="edit:${tomorrow}"]`).should('exist')
  })

  it('does not clone in the past on an empty day', () => {
    const today = formatDateStandard(new Date())
    const yesterday = formatDateStandard(getYesterday())

    cy.get(`[data-test="emptyDay:${today}"]`).click()
    cy.get(`[data-test="clone:${today}"]`).click()

    cy.get('input[type="date"]').type(`${yesterday}`)

    cy.get('[data-test="submit"]').should('be.disabled')
    cy.get('.error').contains('Data asta e în trecut. Te rog alege altă dată.')
  })

  it('does not clone in the past on a day with a lesson', () => {
    setupLocalStorage()
    const today = formatDateStandard(new Date())

    cy.get(`[data-test="emptyDay:${today}"]`).click()
    cy.get(`[data-test="clone:${today}"]`).click()

    cy.get('input[type="date"]').type(`2023-07-09`)

    cy.get('[data-test="submit"]').should('be.disabled')
    cy.get('.error').contains('Data asta e în trecut. Te rog alege altă dată.')
  })

  it('clones successfully in the next week', () => {
    // use getTomorrow to get the date
    const today = formatDateStandard(new Date())
    const sameDayNextWeek = formatDateStandard(getSameDayNextWeek())

    cy.get(`[data-test="emptyDay:${today}"]`).click()
    cy.get(`[data-test="clone:${today}"]`).click()

    cy.get('input[type="date"]').type(`${sameDayNextWeek}`)
    cy.get('[data-test="submit"]').click()
    //TODO   to do navigation to next week
  })

  it('clones a past lesson', () => {
    // use getTomorrow to get the date
    setupLocalStorage()

    const today = formatDateStandard(new Date())

    cy.get(':nth-child(2) > custom-select')
      .shadow()
      .within((item) => {
        cy.get('[class="icon secondary"]').click()
        cy.get('[data-value="6"]').click()
      })

    cy.get(':nth-child(3) > custom-select')
      .shadow()
      .within((item) => {
        cy.get('[class="icon secondary"]').click()
        cy.get('[data-value="2"]').click()
      })

    cy.get('[data-test="clone:2023-07-06"]').click()
    cy.get('input[type="date"]').type(`${today}`)
    cy.get('[data-test="submit"]').click()

    //TODO   can't navigate back to current date
  })
})
