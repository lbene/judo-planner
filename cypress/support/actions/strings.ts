export function generateRandomString(): string {
  return crypto.randomUUID()
}

export function generateArrayOfStrings(length: number): string[] {
  const result = []

  for (let i = 0; i < length; i++) {
    result[i] = generateRandomString()
  }

  return result
}
