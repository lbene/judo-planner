import { type Technique, techniquesStorageId } from '$lib/lessons/domain/techniques'
import { transformToTechniques } from '$lib/lessons/storage/techniquesLocalStorage'

export function setupTechniques(): void {
  window.localStorage.setItem(
    'techniques',
    JSON.stringify([
      {
        id: 'empty_technique',
        name: 'Szünet',
      },
      {
        id: 'empty_technique',
        name: 'Empty technique',
      },
      {
        id: 'video_technique',
        name: 'Video technique',
        video: 'https://www.youtube.com/watch?v=-Dp7MDPYfUI',
      },
      {
        id: 'image_technique',
        name: 'Image technique',
        video: 'https://www.english-efl.com/wp-content/uploads/2019/12/test.jpg',
      },
    ])
  )
}

export function addWazaElement(name: string, media?: string) {
  // Cypress works in a way that whatever we call with the cy object is just schedule to run at a later date
  // so it's not actually run at the moment of the function call.

  // Every function in this chain returns a Chainable object so that we can declare what needs to be called in what
  // order. Cypress will make sure that the order of execution is correct.
  return addTechnique(name, media)
    .then(() => cy.getAllLocalStorage())
    .then(getLocalStorageForCurrentUrl)
    .then(getTechniques)
    .then(getLatestTechniqueId)
}

function addTechnique(name: string, media: string | undefined) {
  cy.get('[data-test="addTech"]').click()
  cy.get('[data-test="formModal"]').within(() => {
    cy.get('input[name="name"]').type(name)
    if (media) {
      cy.get('input[name="videoUrl"]').type(media)
    }
  })

  // We need to return a Chainable here, so that the result of this can be chained together in the next
  // .then() call
  return cy.get('[data-test="submit"]').click()
}

function getLocalStorageForCurrentUrl(storage: Cypress.StorageByOrigin) {
  const baseUrl = Cypress.config('baseUrl')

  if (baseUrl) {
    // Does not return here, because the actual return happens in the anonymous function of the first .then()
    cy.wrap(storage[baseUrl])
  }
}

function getTechniques(storage: Cypress.StorableRecord) {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  const storageContent = storage[techniquesStorageId] as string

  // We need to return a Chainable here, so that the result of this can be chained together in the next
  // .then() call
  return cy.wrap(transformToTechniques(storageContent))
}

function getLatestTechniqueId(techniques: Technique[]) {
  if (techniques.length === 0) {
    throw new Error('Somehow the break was deleted and that should not be possible')
  }

  // We need to return a Chainable here, so that the result of this can be chained together in the next
  // .then() call
  return cy.wrap(techniques[techniques.length - 1].id)
}
