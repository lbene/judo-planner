export function formatDateStandard(date: Date): string {
  const month = date.getMonth() + 1
  const day = date.getDate()

  const monthString = month < 10 ? `0${month}` : month
  const dayString = day < 10 ? `0${day}` : day

  return `${date.getFullYear()}-${monthString}-${dayString}`
}

export function getTomorrow(): Date {
  const tomorrow = new Date()
  tomorrow.setDate(tomorrow.getDate() + 1)

  return tomorrow
}

export function getYesterday(): Date {
  const yesterday = new Date()
  yesterday.setDate(yesterday.getDate() - 1)

  return yesterday
}

export function getSameDayNextWeek(): Date {
  const sameDayNextWeek = new Date()
  sameDayNextWeek.setDate(sameDayNextWeek.getDate() + 7)

  return sameDayNextWeek
}
