import localStorage from '../../fixtures/localstorage.json'

export function setupLocalStorage(): void {
  for (const item of Object.entries(localStorage)) {
    window.localStorage.setItem(item[0], JSON.stringify(item[1]))
  }
}
