export function searchInTechniqueModal(name: string) {
  cy.get(`[data-test="modal"]`).within((item) => {
    cy.get(`[name="search"]`).click().clear().type(name)
  })
}
