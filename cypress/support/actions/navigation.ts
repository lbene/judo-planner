export function visitTechniquesPage(languageCode: string) {
  cy.visit(`/${languageCode}/settings/technique`)
}

export function visitHomePage(languageCode: string) {
  cy.visit(`/${languageCode}`)
}
